this.ConfigLoader = {
    get: function(key){
        var configObject = Config.findOne({'key': key});
        if( !!configObject && !!configObject.value ) {
            return configObject.value;
        } else {
            return false;
        }
    }
};
