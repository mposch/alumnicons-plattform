Meteor.methods({
    fetchAllAlumniRoles: function () {
        return AlumniRoles.find({}, {sort: {'name': 1}}).fetch();
    }
});