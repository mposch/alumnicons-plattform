Meteor.methods({
    fetchAllHubs: function () {
        return fetchHubs({'position': {$exists: false}}, {sort: {position: 1, name: 1}});
    },
    fetchAllSuperiorHubs: function(){
        return fetchHubs({'position': {$exists: true}}, {sort: {position: 1}});
    }
});

function fetchHubs(selector, options){
    var hubs = Hubs.find(selector, options).fetch();

    var isFirst = true;
    hubs.forEach(function(hub) {
        hub.alumniCount = Alumni.find({'hub._id':hub._id}).count();
        if(hub.alumniCount === 0){
            delete hub.alumniCount;
        }
        if(isFirst) {
            hub.isFirst = true;
            isFirst = false;
        }
    });

    return hubs;
}
