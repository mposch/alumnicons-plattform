Meteor.methods({
    fetchAllAlumni: function () {
        return Alumni.find({}, {sort: {'lastname': 1}, reactive: false}).fetch();
    },
    fetchAllApprovedAlumni: function () {
        return Alumni.find({'toBeApproved': false}, {sort: {'lastname': 1}, reactive: false}).fetch();
    },
    findByUserId: function (userId) {
        return Alumni.findOne({userId: userId});
    },
    getAlumniCount: function () {
        return Alumni.find({}, {reactive: false}).count();
    },
    getClassPresidents: function(){
        //console.log(Alumni.find({'isClassPresident': true}, {reactive: false}));
        return Alumni.find({'isClassPresident': true}, {reactive: false}).fetch();
    },
    getAlumn: function(id) {
        //console.log('getAlumn >>', moment().format('HH:mm:ss:SSS'), id);
        var alumn = Alumni.findOne({'_id': id}, {reactive: true});
        if (alumn) {
            var classPresident      = Alumni.findOne({'_id': alumn.classPresident});
            if(!!classPresident) {
                alumn.classPresident = {
                    _id: classPresident._id,
                    firstname: classPresident.firstname,
                    lastname: classPresident.lastname
                };
            }
            alumn.fullName          = alumn.firstname + " " + alumn.lastname;
        }
        //console.log('getAlumn <<', moment().format('HH:mm:ss:SSS'), alumn);
        return alumn;
    },
    saveAlumnDummy: function(){
        //console.log('FOOOOO', moment().format('HH:mm:ss:SSS'));
    },
    saveAlumn: function(data) {
        console.log('SERVER AlumniService: 1 >>', moment().format('HH:mm:ss:SSS'));

        //console.log('save alumn', data);
        var response;

        if (!!data.location && typeof data.location === 'string') {
            data['location'] = Locations.findOne({_id: data.location});
        }

        var inputRole = data.alumniRole;
        //console.log('inputRole', inputRole);
        if (!!inputRole && inputRole != -1) {
            if(typeof inputRole === 'string') {
                data['alumniRole'] = AlumniRoles.findOne({_id: data.alumniRole});
            } else {
                data['alumniRole'] = inputRole;
            }
            if (data['userId'] !== undefined) {
                Meteor.users.update({'_id': data['userId']}, {$set: {'role': 'president'}});
            }
        } else {
            data['alumniRole'] = null;
            if (data['userId'] !== undefined) {
                Meteor.users.update({'_id': data['userId']}, {$set: {'role': 'member'}});
            }
        }

        //console.log('start saving', moment().format('HH:mm:ss:SSS'));
        console.log('SERVER AlumniService: 2 >>', moment().format('HH:mm:ss:SSS'));
        if (data._id) {
            delete data._id;
            response = Alumni.update({_id: data._id}, data, null);
        } else {
            data.toBeApproved = true;
            response =  Alumni.insert(data);
        }
        //console.log('stop saving', moment().format('HH:mm:ss:SSS'));
        console.log('SERVER AlumniService: 3 >>', moment().format('HH:mm:ss:SSS'));

        return response;
    },
    saveMovie: function(){
        console.log('saveMovie on server');
        return Movies.insert({title: 'Fight Club'});
    }
});
