Meteor.methods({
    fetchAllLocations: function () {
        return Locations.find({}, {sort: {'name': 1}}).fetch();
    },
    getLocation: function (id) {
        return Locations.findOne({_id: id});
    }
});