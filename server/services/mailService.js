var rootUrl = process.env.ROOT_URL;

Meteor.methods({
    newUser: function (id) {
        Alumni.update({'_id': id}, {$set: {'toBeApproved': false}});
        var data = Alumni.findOne({'_id': id});

        if(!data.mailaddress) {
            return false;
        }

        var userId = Accounts.createUser({
            'username': data.mailaddress,
            'email': data.mailaddress
        });

        var role = UserRoles.findOne({'name':'member'});
        Meteor.users.update({'_id':userId}, {$set: {'role': role.name, 'activated': false}});

        Alumni.update({'_id': data._id}, {$set: {'userId': userId}});

        var alumn = Alumni.findOne({'userId':userId});
        if(alumn === undefined || !alumn){
            return false;
        }

        var html = createNewUserHtml(alumn, userId);

        var recipient = data.mailaddress;
        if(ConfigLoader.get('emailToggle') === 'dev'){
            console.log('dev email');
            recipient = ConfigLoader.get('adminEmail');
        }

        console.log('invite User', recipient);
        Meteor.Mandrill.send({
            'to': recipient,
            'from': "Alumnicons Admin <admin@alumni.icons.at>",
            'subject': "Willkommen auf der Alumni Plattform von icons",
            'html': html
        });

        return userId;
    },
    reinvite: function (userId) {
        var user = Meteor.users.findOne({'_id': userId});
        if (user === undefined) {
            return false;
        }

        var alumn = Alumni.findOne({'userId':userId});
        /*if(alumn === undefined || !alumn){
            return false;
        }*/

        var token = randomString();
        var timestamp = moment().format('YYYY-MM-DDTHH:mm:ss');
        Meteor.users.update({'_id':userId}, {$set: {'setPassword': {'token': token, 'timestamp': timestamp}}});

        //MANDRILL VERSION
        var html = '';
        if(!!alumn && alumn.firstname && alumn.lastname){
            html += "Liebe/r " + alumn.firstname + " " + alumn.lastname + "!<br /><br />";
        }
        html += "Willkommen auf der Alumni Plattform von icons!<br><br>Für dich wurde ein Account angelegt. Bitte folge diesem Link um deinen Account zu aktivieren:<br />";
        html += "<a href='"+rootUrl+"activation/"+userId+"/"+token+"'>";
        html += rootUrl+"activation/"+userId+"/"+token+"</a>";
        html += "<br><br>Beste Grüße<br>Alumnicons Admin";

        var recipient = user.emails[0].address;
        if(ConfigLoader.get('emailToggle') === 'dev'){
            console.log('dev email');
            recipient = ConfigLoader.get('adminEmail');
        }

        Meteor.Mandrill.send({
            'to': recipient,
            'from': "Alumnicons Admin <admin@alumni.icons.at>",
            'subject': "Willkommen auf der Alumni Plattform von icons",
            'html': html
        });

        return userId;
    },
    customSetPassword: function (userId, newPassword) {
        Accounts.setPassword(userId, newPassword);
        return userId;
    },
    triggerResetPassword: function (email) {
        var user = Meteor.users.findOne({'username': email});
        if (user === undefined) {
            return false;
        }

        var alumn = Alumni.findOne({'userId':user._id});
        if(alumn === undefined || !alumn){
            return false;
        }

        var token = randomString();
        var timestamp = moment().format('YYYY-MM-DDTHH:mm:ss');
        Meteor.users.update({'_id': user._id}, {$set: {'setPassword': {'token': token, 'timestamp': timestamp}}});

        //MANDRILL VERSION
        var html = "Liebe/r " + alumn.firstname + " " + alumn.lastname + "!<br><br>Bitte folge diesem Link um dein Passwort zurückzusetzen:<br>";
        html += "<a href='"+rootUrl+"resetpassword/"+user._id+"/"+token+"'>";
        html += rootUrl+"resetpassword/"+user._id+"/"+token+"</a>";
        html += "<br><br>Beste Grüße<br>Alumnicons Admin";

        var recipient = email;
        if(ConfigLoader.get('emailToggle') === 'dev'){
            console.log('dev email');
            recipient = ConfigLoader.get('adminEmail');
        }
        console.log('send mail');
        Meteor.Mandrill.send({
            'to': recipient,
            'from': "Alumnicons Admin <admin@alumni.icons.at>",
            'subject': "Passwort zurücksetzen",
            'html': html
        });
    },
    deleteAllAlumni: function(){
        Alumni.remove({});
    },
    getProcessEnv: function(){
        return process.env;
    },
    notifyAdminsForNewUser: function (alumnData) {
        var html = "Dies ist eine automatische Benachrichtigung über einen neuen Alumni Eintrag!<br><br>";
        html += "Vorname:&nbsp;"+alumnData.firstname+"<br>";
        html += "Nachname:&nbsp;"+alumnData.lastname+"<br>";
        html += "Mailadresse:&nbsp;"+alumnData.mailaddress+"<br>";
        html += "Telephone:&nbsp;"+alumnData.telephone+"<br>";
        html += "<br>Erstellt am:&nbsp;"+alumnData.createdAt+"<br>";

        html += "<br><br>Dein Admin";

        var recipients = [ConfigLoader.get('adminEmail')];
        if(ConfigLoader.get('sendNotifications') === 'true'){
            console.log('dev email');
            recipients.push(ConfigLoader.get('vorstandsVerteiler'));
        }

        //console.log('recipients', recipients);

        //TODO LOGGING an wen verschickt wurde

        recipients.forEach(function(recipient){
            //console.log('recipient mail: ', recipient);
            Meteor.Mandrill.send({
                'to': recipient,
                'from': "Alumnicons Admin <admin@alumni.icons.at>",
                'subject': "Automatische Benachrichtigung: Neuer Alumni Eintrag",
                'html': html
            });
        });

        console.log("notification sent");
    },
    notifyAdminsAboutApproval: function(alumnId){
        var alumnData = Alumni.findOne({'_id':alumnId});

        var html = "Dies ist eine automatische Benachrichtigung über die Bestätigung eines neuen Alumni Eintrags!";
        html += "<br>Der Eintrag ist ab jetzt in der Alumni Liste sichtbar!<br><br>";
        html += "Vorname:&nbsp;"+alumnData.firstname+"<br>";
        html += "Nachname:&nbsp;"+alumnData.lastname+"<br>";
        html += "Mailadresse:&nbsp;"+alumnData.mailaddress+"<br>";
        html += "Telephone:&nbsp;"+alumnData.telephone+"<br>";
        html += "<br>Erstellt am:&nbsp;"+alumnData.createdAt+"<br>";

        html += "<br><br>Dein Admin";

        var recipients = [ConfigLoader.get('adminEmail')];
        if(ConfigLoader.get('sendNotifications') === 'true'){
            recipients.push(ConfigLoader.get('vorstandsVerteiler'));
        } else {
            console.log('dev email');
        }

        recipients.forEach(function(recipient){
            Meteor.Mandrill.send({
                'to': recipient,
                'from': "Alumnicons Admin <admin@alumni.icons.at>",
                'subject': "Automatische Benachrichtigung: Neuer Alumni Eintrag bestätigt",
                'html': html
            });
        });
    },
    notifyAdminsAboutDenial: function(alumnId){
        var alumnData = Alumni.findOne({'_id':alumnId});

        var html = "Dies ist eine automatische Benachrichtigung über die Ablehnung eines neuen Alumni Eintrags!<br><br>";
        html += "Vorname:&nbsp;"+alumnData.firstname+"<br>";
        html += "Nachname:&nbsp;"+alumnData.lastname+"<br>";
        html += "Mailadresse:&nbsp;"+alumnData.mailaddress+"<br>";
        html += "Telephone:&nbsp;"+alumnData.telephone+"<br>";
        html += "<br>Erstellt am:&nbsp;"+alumnData.createdAt+"<br>";

        html += "<br><br>Dein Admin";

        var recipients = [ConfigLoader.get('adminEmail')];
        if(ConfigLoader.get('sendNotifications') === 'true'){
            recipients.push(ConfigLoader.get('vorstandsVerteiler'));
        } else {
            console.log('dev email');
        }

        recipients.forEach(function(recipient){
            Meteor.Mandrill.send({
                'to': recipient,
                'from': "Alumnicons Admin <admin@alumni.icons.at>",
                'subject': "Automatische Benachrichtigung: Alumni Eintrag abgelehnt",
                'html': html
            });
        });
    },
    notifyAdminsAboutActivation: function(userId){
        var alumnData = Alumni.findOne({'userId':userId});
        console.log('alumnData', userId, alumnData);

        var html = "Folgender Alumni hat gerade seinen Account aktiviert:<br><br>";
        html += "Vorname:&nbsp;"+alumnData.firstname+"<br>";
        html += "Nachname:&nbsp;"+alumnData.lastname+"<br>";
        html += "Mailadresse:&nbsp;"+alumnData.mailaddress+"<br>";
        html += "Telephone:&nbsp;"+alumnData.telephone+"<br>";
        html += "<br>Erstellt am:&nbsp;"+alumnData.createdAt+"<br>";

        html += "<br><br>Dein Admin";

        var recipients = [ConfigLoader.get('adminEmail')];
        if(ConfigLoader.get('sendNotifications') === 'true'){
            recipients.push(ConfigLoader.get('vorstandsVerteiler'));
        } else {
            console.log('dev email');
        }

        recipients.forEach(function(recipient){
            Meteor.Mandrill.send({
                'to': recipient,
                'from': "Alumnicons Admin <admin@alumni.icons.at>",
                'subject': "Automatische Benachrichtigung: Alumni Account Aktivierung",
                'html': html
            });
        });
    }
});

function createNewUserHtml(alumn, userId){
    var token = randomString();
    var timestamp = moment().format('YYYY-MM-DDTHH:mm:ss');
    Meteor.users.update({'_id':userId}, {$set: {'setPassword': {'token': token, 'timestamp': timestamp}}});

    var html = "Liebe/r " + alumn.firstname + " " + alumn.lastname + "!<br><br>Willkommen auf der Alumni Plattform von icons!<br><br>Für dich wurde ein Account angelegt. Bitte folge diesem Link um deinen Account zu aktivieren:<br>";
    html += "<a href='"+rootUrl+"activation/"+userId+"/"+token+"'>";
    html += rootUrl+"activation/"+userId+"/"+token+"</a>";
    html += "<br><br>Beste Grüße<br>Alumnicons Admin";

    return html;
}
