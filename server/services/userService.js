Meteor.methods({
    getUser: function (id) {
        var user = Meteor.users.findOne(id);

        var alumn = Alumni.findOne({userId: user._id});
        if(!!alumn) {
            user.alumn = alumn;
        }

        return user;
    },

    getAllUsers: function() {
        var allUsers = Meteor.users.find({}, {sort:{createdAt:-1}}).fetch();

        allUsers.forEach(function(user){
            user.createdAt = moment(user.createdAt).format('YYYY-MM-DDTHH:mm:ss');
            user.alumn = Alumni.find({userId: user._id});
        });

        return allUsers;
    },

    deleteUser: function(userId){
        var alumn = Alumni.findOne({userId: userId});
        if (!!alumn){
            Alumni.update({'_id': alumn._id}, {$set: {'userId': ''}});
        }

        Meteor.users.remove(userId);

        return true;
    },

    setUserRole: function(userId, role) {
        Meteor.users.update({_id: userId}, {$set: {'role': role}}, null);
    },

    updateUserRole: function (userId, role){
        Meteor.users.update({'_id': userId}, {$set: {'role': role}});
    },

    validatePasswordToken: function(userId, token){
        var user = Meteor.users.findOne(userId);

        if(!!user.setPassword && user.setPassword.token === token){

            var tokenExpiration = moment().subtract(7, 'day');
            if(!!user.setPassword && !!user.setPassword.timestamp && moment(user.setPassword, 'YYYY-MM-DDTHH:mm:ss').isAfter(tokenExpiration)){
                Meteor.users.update({'_id': userId}, {$set: {'setPassword': false}});
                return true;
            }
        }

        return false;
    },

    activateUser: (userId) => {
        Meteor.users.update({'_id': userId}, {$set: {'activated': true}}, null, function(error){
            return error;
        });
    }
});