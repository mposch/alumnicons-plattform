Alumni = new Meteor.Collection("alumni");

Meteor.publish('alumni', function (params) {
    var pointer;
    if(!params) {
        pointer = Alumni.find({}, {reactive:false});
    } else {
        params['reactive'] = false;
        pointer = Alumni.find(params.selector, params.options);
    }
    //console.log('alumni', pointer.fetch().length);
    return pointer;
});

Locations = new Meteor.Collection("locations");
Meteor.publish('locations', function(){return Locations.find();});

AlumniRoles = new Meteor.Collection("alumniRoles");
Meteor.publish('alumniRoles', function(){return AlumniRoles.find();});

Meteor.publish("users", function () {return Meteor.users.find({}, {'emails':1});});

UserRoles = new Meteor.Collection("userRoles");
Meteor.publish('userRoles', function (){return UserRoles.find()});

Actions = new Meteor.Collection("actions");
Meteor.publish('actions', function(){return Actions.find();});

Config = new Meteor.Collection("config");
Meteor.publish('config', function(){return Config.find();});

Snapshots = new Meteor.Collection("snapshots");
Meteor.publish('snapshots', function(){return Snapshots.find();});

Branches = new Meteor.Collection("branches");
Meteor.publish('branches', function(){return Branches.find({}, {sort: {name: 1}})});

Movies = new Meteor.Collection("movies");
Meteor.publish('movies', function(){return Movies.find({}, {sort: {title: 1}})});

Hubs = new Meteor.Collection("hubs");
Meteor.publish('hubs', function(){return Hubs.find({}, {sort: {name: 1}})});

Events = new Meteor.Collection("events");
Meteor.publish('events', function(eventId){
    if(eventId) {
        return Events.find({_id: eventId}, {sort: {date: 1}});
    } else {
        return Events.find({}, {sort: {date: 1}});
    }
});


Meteor.startup(function () {
    Meteor.Mandrill.config({
        'username': "posch.matthias@gmail.com",
        'key': "K5X6O5I5JCSpt3cM-Lzm_w"
    });

    /*for(var i = 0; i < 500; i++){
        var movie = {title: 'Movie '+i};
        Movies.insert(movie);
    }*/
});

getPresidentRecipients = function(){
    var alumniRoles = AlumniRoles.find();
    var alumniRolesQuery = [];
    alumniRoles.forEach(function(alumniRole){
        alumniRolesQuery.push({'alumniRole':alumniRole._id});
    });

    return Alumni.find({$or: alumniRolesQuery}).fetch();
};


Alumni.allow({
    'insert': function (userId, doc) {
        /* user and doc checks ,
         return true to allow insert */
        return true;
    },
    'update': function (userId, doc) {
        /* user and doc checks ,
         return true to allow insert */
        return true;
    },
    'remove': function(userId, doc){
        var user = Meteor.users.findOne(userId);
        return user.role === 'admin';
    }
});

Branches.after.update(function(userId, doc){
    Alumni.update({jobbranch: this.previous}, {$set:{jobbranch:doc}}, {multi: true});
});

Branches.after.remove(function(userId, doc){
    Alumni.update({jobbranch: doc}, {$set:{jobbranch:null}}, {multi: true});
});


Alumni.before.update(function(userId, doc){
    console.log('Alumni Collection: before update >>', moment().format('HH:mm:ss:SSS'));
});

Alumni.after.update(function(userId, doc){
    console.log('Alumni Collection: after update >>', moment().format('HH:mm:ss:SSS'));
});


this.randomString = function(){
    var length = 32;
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}