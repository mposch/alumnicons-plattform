Template.navigation.helpers({
    name: function(){
        var user = AuthService.getUser();
        if(!user || !user.alumn) {
            return false;
        }

        return user.alumn.firstname + " " + user.alumn.lastname;
    },
    username: function(){
        var user = AuthService.getUser();
        if(user){
            return user.username;
        } else {
            return false;
        }
    },
    alumniId: function(){
        var user = AuthService.getUser();
        if(user && user.alumn){
            return user.alumn._id;
        } else {
            return "";
        }
    },
    loginLabel: function(){
        if(Meteor.user()){
            return "Logout";
        } else {
            return "Login";
        }
    },
    isAdmin: function(){
        return AuthService.hasRole(Roles.admin);
    },
    isLoggedIn: function(){
        if(Meteor.user()){
            return true;
        } else {
            return false;
        }
    },
    maySeeAdministration: function(){
        return AuthService.may('administrate');
    },
    mayRegisterAlumni: function(){
        return AuthService.hasRole(Roles.hr);
    }
});

Template.navigation.events = {
    'click .login': function (e) {
        if(Meteor.user()){
            Meteor.logout();
        }
        Router.go('/login');
    },
    'click .mobile-menu': function(e){
        e.preventDefault();

        $('.menu-top').toggleClass('menu-top-click');
        $('.menu-middle').toggleClass('menu-middle-click');
        $('.menu-bottom').toggleClass('menu-bottom-click');

        $('.mobile-nav').toggleClass('active');
    },
    'click .nav-item': function(e){
        $('.menu-top').toggleClass('menu-top-click');
        $('.menu-middle').toggleClass('menu-middle-click');
        $('.menu-bottom').toggleClass('menu-bottom-click');

        $('.mobile-nav').toggleClass('active');
    }
}


function testScroll(e){
    e.preventDefault();
    if($(window).scrollTop() > 163){
        $('.fixed-list-header').removeClass('is-hidden');
    } else {
        $('.fixed-list-header').addClass('is-hidden');
    }
}
window.onscroll=testScroll;