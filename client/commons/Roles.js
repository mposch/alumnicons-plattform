this.Roles = {
    hr: {
        slug: 'hr',
        permissions: [
            'createAlumni'
        ]
    },
    iconsBoard: {
        slug: 'iconsBoard',
        permissions: [
            'createAlumni'
        ]
    },
    member: {
        slug: 'member',
        permissions: [
            'viewProfile'
        ]
    },
    classPresident: {
        slug: 'classPresident',
        permissions: [
            'viewProfile',
            'editProfile',
            'inviteAlumn',
            'editAlumniconsInfo',
            'seeEditProfileShortCut',
            'createAlumni'
        ]
    },
    president: {
        slug: 'president',
        permissions: [
            'viewProfile',
            'editProfile',
            'inviteAlumn',
            'editAlumniconsInfo',
            'seeEditProfileShortCut',
            'createAlumni',
            'administrate'
        ]
    },
    admin: {
        slug: 'admin',
        permissions: [
            'viewProfile',
            'editProfile',
            'inviteAlumn',
            'editAlumniconsInfo',
            'csvImport',
            'seeEditProfileShortCut',
            'createAlumni',
            'administrate'
        ]
    }
};