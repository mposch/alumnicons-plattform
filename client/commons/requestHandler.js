//was experimental and is not used anymore

RequestHandler = {
    call: function (serviceName, params, callback) {
        Meteor.call(serviceName, params, function (error, response) {
            if (!!error) {
                console.error(JSON.stringify(error));
            }
            if(!!callback) callback(response);
            Session.set(serviceName, response);
        });
        return Session.get(serviceName);
    }
};