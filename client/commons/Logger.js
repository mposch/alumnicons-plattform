this.Logger = {
    debug: function (logMessage) {
        Meteor.call('getProcessEnv', function (error, processEnv) {
            if(processEnv.NODE_ENV === 'development'){
                console.debug(logMessage);
            }
        });
    },
    info: function (logMessage) {
        console.info(logMessage);
    },
    log: function (logMessage) {
        console.log(logMessage);
    },
    error: function (logMessage, object) {
        console.error(logMessage, object);
    }
}
