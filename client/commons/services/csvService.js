this.CsvService = {
    export: function () {
        ActionService.store('action', 'Alumni exportieren');
        var alumni = AlumniService.fetchAlumni();

        //var data = [['name1', 'city1', 'some other info'], ['name2', 'city2', 'more info']];
        var csvContent = '';

        const columns = [
            {
                title: 'ID',
                property: '_id'
            },
            {
                title: 'Vorname',
                property: 'firstname'
            },
            {
                title: 'Nachname',
                property: 'lastname'
            },
            {
                title: 'Standort',
                property: 'location',
                parsingFn: function(value) {
                    return value.name;
                }
            },
            {
                title: 'Jahrgangssprecher',
                property: 'classPresident',
                parsingFn: function(value) {
                    if(!!value && value !== -1){
                        var classPresident = Alumni.findOne(value);
                        if(!!classPresident) {
                            return classPresident.firstname + ' ' + classPresident.lastname;
                        } else {
                            return '';
                        }
                    } else {
                        return '';
                    }
                }
            },
            {
                title: 'Mailadresse',
                property: 'mailaddress'
            },
            {
                title: 'Alternative Mailadresse',
                property: 'alternativemailaddress'
            },
            {
                title: 'Telefonnummer',
                property: 'telephone'
            },
            {
                title: 'Alternative Telefonnummer',
                property: 'alternativetelephone'
            },
            {
                title: 'Adresse',
                property: 'address'
            },
            {
                title: 'Arbeitgeber',
                property: 'employer'
            },
            {
                title: 'Job',
                property: 'job'
            },
            {
                title: 'Branche',
                property: 'jobbranch',
                parsingFn: function(value) {
                    return value.name;
                }
            },
            {
                title: 'Job Seit',
                property: 'jobSince'
            },
            {
                title: 'icons Start',
                property: 'iconsStart'
            },
            {
                title: 'icons Ende',
                property: 'iconsEnd'
            },
            {
                title: 'Vereinsmitglied',
                property: 'isClubMember',
                parsingFn: function() {
                    return 'X';
                }
            }
        ];

        columns.forEach(function(column){
            csvContent += column.title + ';';
        });
        csvContent += '\n';

        alumni.forEach(function (alumn) {
            var dataString = '';

            columns.forEach(function(column){
                //console.log('column', column);
                if(!!alumn[column.property]){
                    //console.log('alumn[column.property]', alumn[column.property]);
                    if(!!column.parsingFn){
                        dataString += column.parsingFn(alumn[column.property]);
                    } else {
                        dataString += alumn[column.property];
                    }
                }
                dataString += ';';
            });

            dataString = dataString.substring(0, dataString.length-1);
            csvContent += dataString + '\n';

            /*dataString += alumn._id;
            dataString += ';';

            if (alumn.firstname) {
                dataString += alumn.firstname
            }
            dataString += ';';
            if (alumn.lastname) {
                dataString += alumn.lastname;
            }
            dataString += ';';
            if (alumn.location && alumn.location.name) {
                dataString += alumn.location.name;
            }
            dataString += ';';
            if (alumn.classPresident && alumn.classPresident != -1) {
                var classPresident = Alumni.findOne(alumn.classPresident);

                dataString += classPresident.firstname + ' ' + classPresident.lastname;
            }
            dataString += ';';
            if (alumn.mailaddress) {
                dataString += alumn.mailaddress;
            }
            dataString += ';';
            if (alumn.alternativemailaddress) {
                dataString += alumn.alternativemailaddress;
            }
            dataString += ';';
            if (alumn.telephone) {
                dataString += alumn.telephone;
            }
            dataString += ';';
            if (alumn.alternativetelephone) {
                dataString += alumn.alternativetelephone;
            }
            dataString += ';';
            if (alumn.address) {
                dataString += alumn.address;
            }
            dataString += ';';
            if (alumn.employer) {
                dataString += alumn.employer;
            }
            dataString += ';';
            if (alumn.job) {
                dataString += alumn.job;
            }
            dataString += ';';
            if (alumn.jobbranch) {
                dataString += alumn.jobbranch.name;
            }
            dataString += ';';
            if (alumn.jobSince) {
                dataString += alumn.jobSince;
            }
            dataString += ';';
            if (alumn.iconsStart) {
                dataString += alumn.iconsStart;
            }
            dataString += ';';
            if (alumn.iconsEnd) {
                dataString += alumn.iconsEnd;
            }

            dataString += ';';
            if (alumn.isClubMember) {
                dataString += 'X';
            }
            //dataString += ';';
            csvContent += dataString + '\n';*/
        });

        //var encodedUri = encodeURI(csvContent);

        //var link = document.createElement('a');
        //link.setAttribute('href', encodedUri);

        var date = new Date();
        var filename = 'alumnicons_export_' + date.getFullYear() + this.getMonth(date.getMonth()) + date.getDate() + '.csv'
        //link.setAttribute('download', filename);

        //link.click();
        //window.open(encodedUri);

        this.download(csvContent, filename, 'text/csv');
    },
    download: function (strData, strFileName, strMimeType) {
        var D = document,
            a = D.createElement('a');
        strMimeType = strMimeType || 'application/octet-stream';


        if (navigator.msSaveBlob) { // IE10
            return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
        }
        /* end if(navigator.msSaveBlob) */


        if ('download' in a) { //html5 A[download]
            a.href = 'data:' + strMimeType + ',' + encodeURIComponent(strData);
            a.setAttribute('download', strFileName);
            a.innerHTML = 'downloading...';
            D.body.appendChild(a);
            setTimeout(function () {
                a.click();
                D.body.removeChild(a);
            }, 66);
            return true;
        }
        /* end if('download' in a) */


        //do iframe dataURL download (old ch+FF):
        var f = D.createElement('iframe');
        D.body.appendChild(f);
        f.src = 'data:' + strMimeType + ',' + encodeURIComponent(strData);

        setTimeout(function () {
            D.body.removeChild(f);
        }, 333);
        return true;
    },
    getMonth: function (monthDigit) {
        var month = monthDigit++;
        month = month.toString();

        if (month.length === 1) {
            month = '0' + month;
        }

        return month;
    }
};