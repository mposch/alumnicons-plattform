this.AlumniRoleService = {
    fetchAll: function(){
        Meteor.call('fetchAllAlumniRoles', function (error, result) {
            if(!error) {
                Session.set('fetchAllAlumniRoles', result);
            }
        });
        return Session.get('fetchAllAlumniRoles');
    }
};