this.UserService = {
    findOne: function(id){
        return Meteor.users.findOne(id);
    },
    fetchAllRoles: function(){
        return UserRoles.find().fetch();
    },
    fetchAllUsers: function(){
        var allUsers = Meteor.users.find({}, {sort:{createdAt:-1}}).fetch();

        allUsers.forEach(function(user){
            user.createdAt = moment(user.createdAt).format('YYYY-MM-DDTHH:mm:ss');
            user.alumn = Alumni.findOne({userId: user._id});
        });

        return allUsers;
    },
    deleteById: function(userId) {
        Meteor.call('deleteUser', userId, function(error){
            console.log('deleted user');
            if(!error){
                Notifications.success('User erfolgreich gelöscht');
            }
        });
    },
    setRole: function(userId, role, callback){
        Meteor.call('setUserRole', userId, role, function(error){
            if(!!error){
                console.log('ERROR', error);
                return false;
            }

            if(!!callback) callback();
            Notifications.success('Rolle erfolgreich zugeordnet');
        });
    },
    assignAlumnToUser: function(userId, alumnId, callback){
        var data = {
            userId: userId
        };

        var alumn = AlumniService.findByUserId(userId);
        if(!!alumn) {
            Alumni.update({'_id': alumn._id}, {$set: {'userId': ''}});
        }

        Alumni.update({_id: alumnId}, {$set: data}, null, callback);
        Notifications.success('Alumni erfolgreich zugeordnet');
    },
    setUserPassword: function(userId, password, callback){
        Meteor.call('customSetPassword', userId, password, callback);
        Notifications.success('Passwort erfolgreich gesetzt');
    },
    reinvite: function(userId, callback){
        Meteor.call('reinvite', userId, callback);
        Notifications.success('Einladung erfolgreich verschickt');
    }
};
