this.ActionService = {
    store: function(type, message){
        var userId = Meteor.userId();

        var data = {
            message:    message,
            type:       type,
            userId:     userId,
            createdAt:  moment().format(Formats.DATETIME)
        };

        Actions.insert(data);
    }
};
