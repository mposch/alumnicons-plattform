this.HubService = {
    find: function () {
        return Hubs.find();
    },
    save: function(data){
        if(!data._id){
            Hubs.insert(data);
        } else {
            var id = data._id;
            delete data._id;

            var unset = false;
            for(var property in data){
                if(!data[property]){

                    if(!unset) unset = {};

                    unset[property] = false;
                    delete data[property];
                }
            }

            if(unset){
                Hubs.update(id, {$set: data, $unset: unset});
            } else {
                Hubs.update(id, {$set: data});
            }
        }
    },
    findOne: function(id){
        return Hubs.findOne(id);
    },
    delete: function(id){
        return Hubs.remove(id);
    }
};
