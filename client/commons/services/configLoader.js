this.ConfigLoader = {
    get: function(key){
        var configObject = My2Config.findOne({'key': key});
        if( !!configObject && !!configObject.value ) {
            return configObject.value;
        } else {
            return false;
        }
    }
};