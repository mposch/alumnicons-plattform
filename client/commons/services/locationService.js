this.LocationService = {
    getLatLng: function(address, callback){
        VazcoMaps.init({
            'sensor': true,
            'key': 'AIzaSyA9mm8lBu01VcFQXGIhh6ld_HzUhvoLZuQ'
        }, function () {
            this.mapEngine = VazcoMaps.gMaps();
            this.mapEngine.geocode({
                address: address,
                callback: function (results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        callback({lat: latlng.lat(), lng: latlng.lng()});
                    }
                }
            });
        }.bind(this));
    },
    fetchAllLocations: function(callback){
        Meteor.call('fetchAllLocations', function (error, result) {
            if(!error) {
                Session.set('fetchAllLocations', result);
            }
        });
        return Session.get('fetchAllLocations');
    },
    getLocation: function(){
        Meteor.call('getLocation', function (error, result) {
            if(!error) {
                Session.set('getLocation', result);
            }
        });
        return Session.get('getLocation');
    }
};