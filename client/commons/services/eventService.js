this.EventService = {
    find: function () {
        return Events.find();
    },
    save: function(data){
        if(!data._id){
            Events.insert(data);
        } else {
            var id = data._id;
            delete data._id;

            var unset = false;
            for(var property in data){
                if(!data[property]){

                    if(!unset) unset = {};

                    unset[property] = false;
                    delete data[property];
                }
            }

            if(unset){
                Events.update(id, {$set: data, $unset: unset});
            } else {
                Events.update(id, {$set: data});
            }
        }
    },
    findOne: function(id){
        return Events.findOne(id);
    },
    delete: function(id){
        return Events.remove(id);
    }
};
