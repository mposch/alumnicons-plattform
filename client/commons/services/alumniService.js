this.AlumniService = {
    fetchAllApprovedAlumni: function (callback){
        Meteor.call('fetchAllApprovedAlumni', function(error, response) {
            if(!error) {
                Session.set('fetchAllRawAlumni', response);
            }

            if(!!callback) callback(response);
        });
        return Session.get('fetchAllRawAlumni');
    },
    fetchAllRawAlumni: function (callback){
        Meteor.call('fetchAllAlumni', function(error, response) {
            if(!error) {
                Session.set('fetchAllRawAlumni', response);
            }

            if(!!callback) callback(response);
        });
        return Session.get('fetchAllRawAlumni');
    },
    fetchCurrentAlumn: function () {
        Meteor.call('getAlumn', Session.get('alumniId'), function (error, result) {
            Session.set('alumn', result);
        });
        return Session.get('alumn');
    },

    fetchClassPresidents: function () {
        Meteor.call('getClassPresidents', function (error, result) {
            Session.set('classPresidents', result);
        });
        return Session.get('classPresidents');
    },

    findByUserId: function (userId, callback) {
        Meteor.call('findByUserId', userId, function (error, result) {
            Session.set('findByUserId', result);
            if(!!callback) callback(result);
        });
        return Session.get('findByUserId');
    },

    findById: function (id, callback) {
        //console.log('AlumniService: findById 1 >>', moment().format('HH:mm:ss:SSS'), id);
        return Alumni.findOne(id, callback);
        /*Meteor.call('getAlumn', id, function (error, result) {
            if(!error) {
                Session.set('findById', result);
            }

            console.log('AlumniService: findById 2 >>', moment().format('HH:mm:ss:SSS'));
            if(!!callback) callback(result);
        });
        return Session.get('findById');*/
    },

    fetchAlumniToBeApproved: function () {
        return this.fetchAlumni({selector: {'toBeApproved': true}});
    },

    fetchApprovedAlumni: function () {
        return this.fetchAlumni({selector: {'toBeApproved': {$ne: true}}});
    },

    fetchAlumniForAdministration: function () {
        var alumni = AlumniService.fetchApprovedAlumni();
        alumni = this.loadAlumniData(alumni);
        return alumni;
    },

    fetchAlumni: function (params) {
        var alumni = Alumni.find().fetch();

        alumni.sort(sortAlumni);

        alumni = this.loadAlumniData(alumni, Session.get('stringFilter'));

        //if (!!params) {
        //    if (!params.fetchRaw) {
        //        alumni = this.loadAlumniData(alumni, Session.get('stringFilter'));
        //    }
        //} else {
        //    alumni = this.loadAlumniData(alumni, Session.get('stringFilter'));
        //}

        Session.set('lockAlumniFetching', false);
        return alumni;
    },

    fetchAllAlumni: function(){
        var alumni = Alumni.find().fetch();
        alumni.sort(sortAlumni);
        alumni = this.loadAlumniData(alumni);
        return alumni;
    },

    loadAlumniData: function (alumni, filter) {
        var _this = this;
        var filteredAlumni = [];

        _.each(alumni, function (alumn) {
            var displayAlumni = false;
            if (!!filter) {
                var fullname        = alumn.firstname + ' ' + alumn.lastname;
                var telephone       = alumn.telephone ? alumn.telephone : '';
                var mailaddress     = alumn.mailaddress ? alumn.mailaddress : '';
                var address         = alumn.address ? alumn.address : '';
                var employer        = alumn.employer ? alumn.employer : '';
                var job             = alumn.job ? alumn.job : '';
                if (_this.findString(fullname, filter)
                    || _this.findString(telephone, filter)
                    || _this.findString(mailaddress, filter)
                    || _this.findString(address, filter)
                    || _this.findString(employer, filter)
                    || _this.findString(job, filter)) {
                    displayAlumni = true;
                }
            } else {
                displayAlumni = true;
            }

            if (displayAlumni) {
               /* //if (alumn.location) {
                //    alumn.location = Locations.findOne({'_id': alumn.location});
                //}
                if (alumn.alumniRoles) {
                    alumn.alumniRole = AlumniRoles.findOne({'_id': alumn.alumniRole});
                }
                if (alumn.classPresident) {
                    alumn.classPresident = Alumni.findOne({'_id': alumn.classPresident});
                }

                if (!alumn.userId) {
                    alumn.status = "not-registered";
                } else {
                    var user = Meteor.users.find({_id: alumn.userId}).fetch();
                    if (!user[0]) {
                        alumn.status = "not-registered";
                    } else if (user[0].activated) {
                        alumn.status = "verified";
                    } else {
                        alumn.status = "registered";
                    }
                }

                if (alumn.isClubMember) {
                    alumn.membership = "member";
                } else {
                    alumn.membership = false;
                }

                alumn.mayEditProfile = AuthService.may(Permissions.EDITPROFILE, alumn._id);
                alumn.maySeeEditProfileShortCut = AuthService.may(Permissions.SEEEDITPROFILESHORTCUT, alumn._id);

                alumn.isAdmin = AuthService.hasRole('admin');*/

                //TODO check for performance impact
                //alumn.mayEditProfile = AuthService.may(Permissions.EDITPROFILE, alumn._id);
                //alumn.maySeeEditProfileShortCut = AuthService.may(Permissions.SEEEDITPROFILESHORTCUT, alumn._id);
                alumn.isAdmin = AuthService.hasRole('admin');

                filteredAlumni.push(alumn);
            }
        });

        return filteredAlumni;
    },

    findString: function (string1, string2) {
        if(!!string1) {
            if (typeof string1 === 'object') {
                string1 = string1.toString();
            }
            string1 = string1.replace(' ', '').toLowerCase();
        }

        if(!!string2) {
            if (typeof string2 === 'object') {
                string2 = string2.toString();
            }
            string2 = string2.replace(' ', '').toLowerCase();
        }

        if(string1.indexOf(string2) > -1) {
            return true;
        } else {
            return false;
        }

    },

    remove: function (alumnId) {
        return Alumni.remove(alumnId);
    },

    printTable: function (file) {
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function (event) {
            var csv = event.target.result;

            var data = Papa.parse(csv, {delimiter: '\t'}).data;
            var html = '';
            for (var i = 0; i < data.length; i++) {
                html += '<tr>\r\n';
                for (var item in data[i]) {

                    html += '<td>' + data[i][item] + '</td>\r\n';
                }
                html += '</tr>\r\n';
            }
            $('#contents').html(html);
        };
        reader.onerror = function () {
            alert('Unable to read ' + file.fileName);
        };
    },

    importData: function (file) {
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function (event) {
            var csv = event.target.result;
            var data = Papa.parse(csv, {delimiter: '\t'}).data;
            var alumni = [];

            var index = 0;
            var maxIndex = data.length;
            var firstRow = true;
            setInterval(function(){
                var alumnData = data[index];

                if (!firstRow && !isEmptyStringArray(alumnData)) {
                    if (index > maxIndex) {
                        return;
                    }

                    var alumn = {};
                    alumn.firstname                 = alumnData[2];
                    alumn.lastname                  = alumnData[3];
                    alumn.mailaddress               = alumnData[4].toLowerCase();
                    alumn.alternativemailaddress    = alumnData[5].toLowerCase();
                    alumn.linkedin                  = alumnData[6];
                    alumn.xing                      = alumnData[7];
                    alumn.address                   = alumnData[8];
                    alumn.telephone                 = alumnData[9];
                    alumn.iconsStart                = alumnData[10];
                    alumn.iconsEnd                  = alumnData[11];
                    alumn.employer                  = alumnData[12];
                    alumn.job                       = alumnData[13];
                    alumn.jobSince                  = alumnData[14];

                    // Innsbruck
                    //alumn.location                  = 'KD5Dg7arnzd4WQtYX';

                    // Salzburg
                    alumn.location                  = 'fmxe498yQBTWKX96n';

                    alumni.push(alumn);

                    //console.log('saved successfully', alumn);
                    AlumniService.saveAlumn(alumn, {sendNotifications:false}, function(){
                        //console.log('saved successfully', alumn.firstname, alumn.lastname);
                    });
                } else {
                    firstRow = false;
                }

                ++index;
            }, 500);
        };
        reader.onerror = function () {
            alert('Unable to read ' + file.fileName);
        };
    },

    approveAlumni: function (id) {
        Alumni.update({'_id': id}, {$set: {'toBeApproved': false}});
    },
    setAlumnMemberShips: function (id, memberShip) {
        Alumni.update({'_id': id}, {$set: {'memberShip': memberShip}});
    },
    setClubMembership: function (id, isClubMember) {
        Alumni.update({'_id': id}, {$set: {'isClubMember': isClubMember}});
    },
    removeAlumni: function (id) {
        Alumni.remove({'_id': id});
    },

    countMembers: function (alumni) {
        var members = 0;
        _.map(alumni, function (alumn) {
            if (!!alumn.isClubMember) {
                members++;
            }
        });

        return members;
    },

    saveAlumn: function (alumnData, options, callback) {
        //console.log('AlumniService: saveAlumn 1 >>', moment().format('HH:mm:ss:SSS'), alumnData);
        if (alumnData._id) {
            var alumn = this.findById(alumnData._id);
            persistAlumn(alumn, alumnData, callback);
        } else {
            persistAlumn({}, alumnData, callback);
        }
    }
};

function sortAlumni(a, b) {
    if (a.lastname < b.lastname) return -1;
    if (a.lastname > b.lastname) return 1;
    return 0;
}

function isEmptyStringArray(array){
    var isEmpty = true;
    _.each(array, function(entry) {
        if(entry.length > 0){
            isEmpty = false;
        }
    });

    return isEmpty;
}

function persistAlumn (alumn, inputData, callback){
    alumn.updatedAt = moment().format(Formats.DATETIME);
    if (!alumn.createdAt) {
        alumn.createdAt = moment().format(Formats.DATETIME);
    }

    if (AuthService.hasRole(Roles.hr)) {
        alumn.toBeApproved = true;
    }

    var origAddress = alumn['address'];
    for (var propertyName in inputData) {
        alumn[propertyName] = inputData[propertyName];
    }


    if (alumn['address'] && origAddress !== inputData['address']) {
        LocationService.getLatLng(alumn['address'], function (latlng) {
            alumn.lat = latlng.lat;
            alumn.lng = latlng.lng;

            persistViaCollection(alumn, callback);

            /*Meteor.call('saveAlumn', alumn, function (error) {
                if (!error) {
                    if (!!callback) callback(alumn._id);
                } else {
                    console.log("ERROR", error);
                }
            });*/
        });
    } else {
        //console.log('AlumniService: save without address >>', moment().format('HH:mm:ss:SSS'));
        persistViaCollection(alumn, callback);

        /*Meteor.call('saveAlumn', alumn, function (error) {
            if (!error) {
                if (!!callback) callback(alumn._id);
            } else {
                console.log("ERROR", error);
            }
        });*/
    }
}

function persistViaCollection(data, callback){

    if (!!data.location && typeof data.location === 'string') {
        data['location'] = Locations.findOne({_id: data.location});
    }
    if (!!data.jobbranch && typeof data.jobbranch === 'string') {
        data['jobbranch'] = Branches.findOne({_id: data.jobbranch});
    }
    if (!!data.hub && typeof data.hub === 'string') {
        data['hub'] = Hubs.findOne({_id: data.hub});
    }

    var inputRole = data.alumniRole;
    if (!!inputRole && inputRole != -1) {
        if(typeof inputRole === 'string') {
            data['alumniRole'] = AlumniRoles.findOne({_id: data.alumniRole});
        } else {
            data['alumniRole'] = inputRole;
        }
        if (data['userId'] !== undefined) {
            Meteor.call('updateUserRole', data['userId'], 'president');
        }
    } else {
        data['alumniRole'] = null;
        if (data['userId'] !== undefined) {
            Meteor.call('updateUserRole', data['userId'], 'member');
        }
    }

    if (data._id) {
        var _id = data._id;
        cleanDataObject(data);
        //console.log('AlumniService: update >>', moment().format('HH:mm:ss:SSS'));

        //TODO why doesn't it work with synchronous updating? changes are not in db without callback

        Alumni.update({_id: _id}, {$set: data}, function(error, result){
            //console.log('AlumniService: update callback, finished updating >>', moment().format('HH:mm:ss:SSS'));
            callback(null, _id);
        });
    } else {
        data.toBeApproved = true;
        cleanDataObject(data);
        //console.log('AlumniService insert >>', moment().format('HH:mm:ss:SSS'));
        var id = Alumni.insert(data);
        callback(null, id);
    }
}

function cleanDataObject(obj) {
    delete obj._id;
    for(var prop in obj){
        if(!prop){
            delete obj[prop];
        }
    }
}