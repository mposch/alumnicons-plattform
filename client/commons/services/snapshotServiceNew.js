this.SnapshotService = {
    save: function(data){
        return Snapshots.insert(data);
    },

    fetchAll: function(){
        return Snapshots.find({}, {sort:{createdAt:-1}}).fetch();
    },

    findOne: function(id){
        return Snapshots.findOne({_id: id});
    }
};
