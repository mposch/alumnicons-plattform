this.AuthService = {
    getUser: function(){
        var userObject = Meteor.user();

        if(!userObject) {
            return userObject;
        }

        if(Session.get('LoggedInAlumn')){
            userObject.alumn = Session.get('LoggedInAlumn');
            return userObject;
        }

        Meteor.call('findByUserId', userObject._id, function(error, response){
            userObject.alumn = response;
            Session.set('userObject', userObject);
            Session.set('LoggedInAlumn', response);
        });
        return Session.get('userObject');
    },
    hasRole: function(requestedRole){
        var user = this.getUser();
        return !!user && !!user.role && user.role === requestedRole.slug;
    },
    hasPermission: function(permissions, permissionToCheck){
        for(var i=0; i<permissions.length; i++){
            if(permissions[i] === permissionToCheck){
                return true;
            }
        }
        return false;
    },
    may: function(requestedPermission, targetAlumnId){
        var user = this.getUser();
        var alumn = !!user ? user.alumn : null;
        var userPermissions = [];

        var targetAlumn = false;
        if (targetAlumnId) { targetAlumn = Alumni.findOne({_id: targetAlumnId}); }

        if(!user) { return false; }

        if(this.hasRole(Roles.admin)){
            this.addPermissions(Roles.admin.permissions, userPermissions);
        } else {
            if (!!alumn && alumn.alumniRole !== null && alumn.alumniRole !== undefined) {
                var role = AlumniRoles.findOne({_id: alumn.alumniRole});
                if(Roles[role.slug]) {
                    this.addPermissions(Roles[role.slug].permissions, userPermissions);
                }
            } else if (!!alumn && alumn.isClassPresident) {
                this.addPermissions(Roles.classPresident.permissions, userPermissions);
            } else if (!!alumn && alumn.isClubMember) {
                this.addPermissions(Roles.member.permissions, userPermissions);
            }

            if(user && user.role && Roles[user.role] !== undefined){
                var permissions = Roles[user.role].permissions;
                this.addPermissions(permissions, userPermissions);
            }

            if (!!alumn && alumn._id === targetAlumnId) {
                Logger.debug("is same alumni");
                Logger.debug(alumn._id, targetAlumnId);
                userPermissions.push('editProfile');
            }

            // is alumn was created by hr user, then he/she may edit the profile, as long as it's not approved
            if(this.hasRole(Roles.hr) && targetAlumn && targetAlumn.createdBy === user._id && targetAlumn.toBeApproved){
                userPermissions.push('editProfile');
            }
        }

        return !!this.hasPermission(userPermissions, requestedPermission);
    },
    addPermissions: function(source, destination){
        for(var i=0; i<source.length; i++){
            destination.push(source[i]);
        }
    },
    isClubMember: function(){
        var user = this.getUser();
        if(!user.alumn){
            return false;
        } else {
            return user.alumn.isClubMember;
        }
    }
};
