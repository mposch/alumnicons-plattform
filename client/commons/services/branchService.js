this.BranchService = {
    fetchBranches: function () {
        var branches = Branches.find();
        return branches;
    },
    save: function(data){
        if(!data._id){
            Branches.insert(data);
        } else {
            var id = data._id;
            delete data._id;
            Branches.update(id, {$set:data});
        }
    },
    findOne: function(id){
        return Branches.findOne(id);
    },
    delete: function(id){
        return Branches.remove(id);
    }
};
