Alumni          = new Meteor.Collection("alumni");
Locations       = new Meteor.Collection("locations");
AlumniRoles     = new Meteor.Collection("alumniRoles");
UserRoles       = new Meteor.Collection("userRoles");
Actions         = new Meteor.Collection("actions");
My2Config       = new Meteor.Collection("config");
Snapshots       = new Meteor.Collection("snapshots");
Branches        = new Meteor.Collection("branches");
Movies          = new Meteor.Collection("movies");
Hubs            = new Meteor.Collection("hubs");
Events          = new Meteor.Collection("events");

//subs = new SubsManager();

Meteor.subscribe('users', function(){
    Session.get('usersLoaded');
});
Meteor.subscribe('userRoles');
Meteor.subscribe('config');

Router.configure({
    layoutTemplate: 'layout'
});

Meteor.startup(function () {
    _.extend(Notifications.defaultOptions, {
        timeout: 4000
    });
});

Blaze._allowJavascriptUrls();

Router.map(function () {
    this.route('movies', {
        path: '/movies',
        template: 'movies',
        waitOn: function() {
            Meteor.subscribe('movies');
            Meteor.subscribe('alumni');
        }
    });
});

Template.movies.helpers({
    movies: function(){
        return Movies.find();
    },
    movieCount: function(){
        return Movies.find().count();
    },

    alumni: function(){
        return Alumni.find();
    },
    alumniCount: function(){
        return Alumni.find().count();
    }

});

Template.movies.events = {
    'click .btn': function(){
        Movies.insert({title:'FightClub'});
    }
};
