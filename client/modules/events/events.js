Template.events.helpers({
    upcomingEvents: function() {
        var events = Events.find({}, {sort:{date: 1}}).fetch();

        return _.filter(events, (event) => {
            return moment().isBefore(moment(event.date, 'DD.MM.YYYY HH:mm').format('YYYY-MM-DD HH:mm'));
        });
    },
    pastEvents: function(){
        var events = Events.find({}, {sort:{date: -1}}).fetch();
        return _.filter(events, (event) => {
            return moment().isAfter(moment(event.date, 'DD.MM.YYYY HH:mm').format('YYYY-MM-DD HH:mm'));
        });
    }
});