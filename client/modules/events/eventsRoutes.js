Router.map(function () {
    this.route('events', {
        path: '/events',
        template: 'events',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('events');
        }
    });
});
