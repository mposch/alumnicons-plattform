Router.map(function () {
    this.route('home', {
        path: '/',
        template: 'alumni',
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            } else {
                ActionService.store('navigate', 'Alumni Seite');
                Router.go('/alumni');
            }
        }
    });

    this.route('alumniList', {
        path: '/alumni',
        template: 'alumni',
        onRun: function () {
            ActionService.store('navigate', 'Alumni Liste');
            if (!Meteor.userId()) {
                Router.go('/login');
            }

            //Session.set('limit', 50);
        },
        waitOn: function(){

            var query = this.params.query;

            var params = {
                selector: {'toBeApproved': false},
                options: {sort: {'lastname': 1}}
            };

            var useLimit = true;

            Session.set('classPresident', '');
            Session.set('location', '');
            Session.set('hubs', '');

            if(!!query.classPresident) {
                params.selector['classPresident'] = query.classPresident;
                Session.set('classPresident', query.classPresident);
                useLimit = false;
            }

            if(!!query.location) {
                params.selector['location._id'] = query.location;
                Session.set('location', query.location);
                useLimit = false;
            }

            if(!!query.hub) {
                params.selector['hub._id'] = query.hub;
                Session.set('hub', query.hub);
                useLimit = false;
            }

            if(!!query.filter) {
                Session.set('stringFilter', query.filter);
                useLimit = false;
            }

            if(useLimit && !!Session.get('limit')){
                params.options['limit'] = Session.get('limit');
            }

            Meteor.subscribe('alumni', params, function(){
                Session.set('alumnilist.ready', true);
            });

            Meteor.subscribe('locations');
            Meteor.subscribe('hubs');
        }
    });

    this.route('alumniCreate', {
        path: 'alumni/create',
        template: 'alumniEdit',
        onRun: function () {
            ActionService.store('navigate', 'Alumni Anlegen');
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('locations');
            Meteor.subscribe('alumniRoles');
            Meteor.subscribe('branches');
            Meteor.subscribe('hubs');
        }
    });

    this.route('/alumni/:_id', {
        name: 'alumniView',
        template: 'alumniView',
        data: function () {
            Session.set('alumniId', this.params._id);
        },
        onRun: function () {
            ActionService.store('navigate', 'Alumni Anzeige');
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            var params = {
                selector: {'_id':this.params._id},
                options: {sort: {'lastname': 1}}
            };
        }
    });

    this.route('alumniEdit', {
        path: '/alumni/:_id/edit',
        template: 'alumniEdit',
        data: function () {
            Session.set('alumniId', this.params._id);
        },
        onRun: function () {
            ActionService.store('navigate', 'Alumni Bearbeitung');
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function() {
            Meteor.subscribe('alumni', {selector: {'_id':Session.get('alumniId')}}, function(){
                Session.set('alumniEdit.alumni.ready', true);
            });
            Meteor.subscribe('branches', function(){
                Session.set('alumniEdit.branches.ready', true);
            });
            Meteor.subscribe('locations', function(){
                Session.set('alumniEdit.locations.ready', true);
            });
            Meteor.subscribe('alumniRoles', function(){
                Session.set('alumniEdit.alumniRoles.ready', true);
            });

            Meteor.subscribe('hubs');
        }
    });

    this.route('worldmap', {
        path: '/worldmap',
        template: 'worldmap',
        onRun: function () {
            ActionService.store('navigate', 'Alumni Weltkarte');
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });

    this.route('import', {
        path: '/alumnimport',
        template: 'import',
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }/* else {
                ActionService.store('navigate', 'Alumni Import');
                console.log('import');
                if (!AuthService.hasRole('admin')) {
                    Router.go('/alumni');
                }
            }*/
        },
        waitOn: function(){
            //console.log('waitOn import');
            Meteor.subscribe('alumni');
        }
    });

    this.route('alumniClearList', {
        path: '/clearalumni',
        template: 'clearalumni',
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('alumni', function(){
                Session.set('clearalumni.ready', true);
            });
        }
    });
});
