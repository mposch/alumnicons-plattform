////////// Alumni //////////
Session.set('locationFilter', -1);
Session.set('presidentFilter', -1);
Session.set('sortBy', 'lastname');
Session.set('ascending', true);

UI.registerHelper('selected', function (a, b) {
    if (a === b) {
        return true;
    }
    return false;
});
