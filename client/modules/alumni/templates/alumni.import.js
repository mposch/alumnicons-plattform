Template.import.events = {
    'change #files': function () {
        var files = $('#files')[0].files; // FileList object
        var file = files[0];

        // read the file metadata
        var output2 = '';
        output2 += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output2 += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output2 += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output2 += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

        // read the file contents
        AlumniService.printTable(file);

        // post the results
        $('#list').empty();
        $('#list').append(output2);
    },

    'click .import-js': function () {
        var files = $('#files')[0].files; // FileList object
        var file = files[0];

        // read the file metadata
        var output2 = '';
        output2 += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output2 += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output2 += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output2 += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

        // read the file contents
        AlumniService.importData(file);

        // post the results
        $('#list').empty();
        $('#list').append(output2);
    }
};
