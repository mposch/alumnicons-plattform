Template.clearalumni.helpers({
    alumni: function () {
        //console.log(moment().format('HH:mm:ss:SSS'));
        if(Session.get('clearalumni.ready')) {
            var alumni = Alumni.find();
            //console.log(alumni.fetch());
            //console.log(moment().format('HH:mm:ss:SSS'));
            return alumni;
        }
    },
    attributes: function() {
        return [
            'Vorname',
            'Nachname',
            'Email',
            'Email (alternativ)',
            'Telefon',
            'Telefon (alternativ)',
            'Adresse',
            'Adresse (alternativ)',
            'Unternehmen',
            'Job',
            'Seit',
            'Linkedin',
            'Xing',
            'icons Start',
            'icons Ende',
            'Standort'
        ];
    }
});