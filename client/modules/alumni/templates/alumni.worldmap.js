Template.worldmap.rendered = function(){
    $('.map-canvas').height($(window).height() - 60);

    AlumniService.fetchAllRawAlumni(function(alumni){
        var marker = [];
        var markerImage = new google.maps.MarkerImage(
            '/marker.png',
            null,
            null,
            null,
            new google.maps.Size(20, 32)
        );
        alumni.forEach(function (alumn) {
            if (alumn.lat !== undefined && alumn.lng !== undefined) {
                marker.push({
                    lat: alumn.lat,
                    lng: alumn.lng,
                    infoWindow: {
                        content: '<a href="/alumni/' + alumn._id + '"><h4>' + alumn.firstname + ' ' + alumn.lastname + '</h4></a><div>' + alumn.address + '</div>'
                    },
                    icon: markerImage
                });
            }
        });

        VazcoMaps.init({}, function () {
            this.mapEngine = VazcoMaps.gMaps();

            this.newMap = new this.mapEngine({
                div: '#map-canvas',
                lat: 48.208174,
                lng: 16.373819,
                zoom: 3
            });

            marker.forEach(function (singleMarker) {
                this.newMap.addMarker(singleMarker);
            });
        });
    }.bind(this));
};