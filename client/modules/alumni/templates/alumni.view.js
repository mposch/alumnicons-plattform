Template.alumniView.helpers({
    /**************************************************************************************************

     DATA

     ***************************************************************************************************/

    alumn       : function () { return AlumniService.fetchCurrentAlumn(); },
    isset       : function (value) { return !!value; },
    errorMsg    : function(){ return Session.get('error-message'); },

    /**************************************************************************************************

     PERMISSIONS

     ***************************************************************************************************/

    isAdmin: function () {
        return AuthService.hasRole('admin');
    },

    mayEdit: function () {
        var mayEdit = AuthService.may('editProfile', Session.get('alumniId'));
        return mayEdit;
    },
    mayInvite: function(){
        if(!AuthService.may('inviteAlumn')){
            return false;
        }
        var alumn = AlumniService.fetchCurrentAlumn();
        if( alumn && (!!alumn.userId || alumn.toBeApproved) ){
            return false;
        } else {
            return true;
        }
    },
    mayChangePassword: function(){
        var alumn = AlumniService.fetchCurrentAlumn();
        return !!alumn && !!alumn.userId && alumn.userId === Meteor.userId();
    },

    showRole: function(){
        var alumn = AlumniService.fetchCurrentAlumn();
        return (alumn
            && (alumn.isClubMember
                || (alumn.alumniRole && alumn.alumniRole.name)
                || alumn.isClassPresident
                || alumn.classPresident
            )
        );
    }
});

Template.alumniView.events = {
    'click .edit-btn': function (e) {
        e.preventDefault();
        Router.go('/alumni/' + Session.get('alumniId') + '/edit');
    },
    'click #inviteModal .action-btn': function (e) {
        e.preventDefault();
        var alumnId = Session.get('inviteAlumnId');
        Meteor.call('newUser', alumnId, function (error) {
            if(error === undefined) {
                $('#inviteModal').modal('hide');
            } else {
                Logger.error("invite failed", error);
            }
        });
    },
    'click .action-btn': function(e){
        e.preventDefault();
        Session.set('inviteAlumnId', $(e.currentTarget).data('id'));
    },
    'click #setPasswordModal .action-btn': function(e){
        e.preventDefault();

        Session.set('error-message', false);

        var currentPassword     = $('#setPasswordModal').find('.current-password-js').val();
        var newPassword         = $('#setPasswordModal').find('.new-password-js').val();
        var confirmPassword     = $('#setPasswordModal').find('.confirm-password-js').val();

        Meteor.loginWithPassword({id:Meteor.userId()}, currentPassword, function(err){
            if (!!err){
                Session.set('error-message', 'Aktuelles Passwort ist nicht korrekt!');
                return;
            }

            if(newPassword !== confirmPassword){
                Session.set('error-message', 'Passwörter stimmen nicht überein!');
                return;
            }

            if(newPassword === '' || confirmPassword === ''){
                Session.set('error-message', 'Alle Felder müssen angegeben werden!');
                return;
            }

            UserService.setUserPassword(Meteor.userId(), newPassword, function(){
                Meteor.loginWithPassword({id:Meteor.userId()}, newPassword);
                $('#setPasswordModal').modal('hide');
            });
        });
    }
};
