Template.alumniEdit.rendered = () => {
    $.getScript("http://platform.linkedin.com/in.js?async=true", function success() {
        IN.init({
            api_key:    '77peif0hynq8n2',
            authorize:  true,
            lang:       'de_DE',
            onLoad:     'OnLinkedInFrameworkLoad'
        })
    });

    OnLinkedInFrameworkLoad = () => {
        console.log('isAuthorized', IN.User.isAuthorized());

        IN.Event.on(IN, "auth", OnLinkedInAuth);
    };

    OnLinkedInAuth = () => {
        const alumni = Alumni.findOne();
        IN.API.Profile(alumni.linkedin).result(ShowProfileData);
        //IN.API.Profile("me").result(ShowProfileData);
    };

    ShowProfileData = (profiles) => {
        var member = profiles.values[0];

        Session.set('linkedInProfile', {
            id:         member.id,
            firstName:  member.firstName,
            lastName:   member.lastName,
            photo:      member.pictureUrl,
            headline:   member.headline
        });
    };
};


Template.alumniEdit.helpers({
    /**************************************************************************************************

     DATA

     ***************************************************************************************************/

    alumn           : function () { console.log('alumni', Alumni.findOne());return !!Alumni.findOne() ? Alumni.findOne() : {}; },
    locations       : function () { return LocationService.fetchAllLocations(); },
    roles           : function () { return AlumniRoleService.fetchAll(); },
    classPresidents : function () { return AlumniService.fetchClassPresidents(); },
    branches        : function () { return BranchService.fetchBranches(); },
    hubs            : function () {
        return Hubs.find();
    },
    linkedInProfile    : function() { return Session.get('linkedInProfile'); },


    /**************************************************************************************************

     RIGHTS

     ***************************************************************************************************/

    mayEditAlumnicons: function () {
        return AuthService.may('editAlumniconsInfo');
    }
});

Template.alumniEdit.events = {
    'click .save-btn': function (e) {
        e.preventDefault();
        var alumnData = {};

        $.each($('.profile-wrapper input'), function () {
            alumnData[this.name] = this.value;
        });

        alumnData._id               = $(e.currentTarget).data('id');
        alumnData.isClubMember      = $('input[name="isClubMember"]').prop('checked');
        alumnData.isClassPresident  = $('input[name="isClassPresident"]').prop('checked');
        alumnData.classPresident    = $('.class-president').val() != -1 ? $('.class-president').val() : '';
        alumnData.location          = $('.location').val() != -1 ? $('.location').val() : '';
        alumnData.jobbranch         = $('.jobbranch').val() != -1 ? $('.jobbranch').val() : '';
        alumnData.alumniRole        = $('.role').val() != -1 ? $('.role').val() : '';
        alumnData.hub               = $('.hub').val() != -1 ? $('.hub').val() : '';

        AlumniService.saveAlumn(alumnData, {sendNotifications: true}, function(error, result){
            Notifications.success('Daten erfolgreich gespeichert');
            Router.go('/alumni/' + result);
        });
    },
    'click .cancel-btn': function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).data('id');
        ActionService.store('action', 'Alumni Bearbeitung abbrechen');
        if (id) {
            Router.go('/alumni/' + $(e.currentTarget).data('id'));
        } else {
            Router.go('alumniList');
        }
    }
};
