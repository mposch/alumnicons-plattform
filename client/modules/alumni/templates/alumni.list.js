//incrementLimit = function(inc) {
//    newLimit = Session.get('limit') + inc;
//    Session.set('limit', newLimit);
//};
//
//Template.alumni.rendered = function() {
//    //console.log('alumni rendered');
//    $(window).scroll(function() {
//        //console.log('SCROLL', Session.get('lockAlumniFetching'));
//        if ($(window).scrollTop() + $(window).height() <= $(document).height() - 400 || Session.get('lockAlumniFetching')) {
//            return;
//        }
//        Session.set('lockAlumniFetching', true);
//        console.log('INCREASE LIMIT', Session.get('limit'));
//        incrementLimit(50);
//    });
//
//    Session.set('alumnilist.showSpinner', true);
//};

Template.alumni.helpers({
    locations       : function () { return Locations.find().fetch(); },
    hubs            : function () { return Hubs.find().fetch(); },
    classPresidents : function () { return AlumniService.fetchClassPresidents(); },
    filterText      : function () { return Session.get('stringFilter'); },
    ascending       : Session.get('ascending'),

    alumniCount: function () {
        return AlumniService.fetchAlumni().length;
    },
    alumniTotalCount: function () {
        Meteor.call('getAlumniCount',
            function(error, result) {
                Session.set('alumniCount', result);
            }
        );
        return Session.get('alumniCount');
    },
    alumni: function () {
        if(Session.get('alumnilist.ready')) {
            var alumni = AlumniService.fetchAlumni();
            if(!alumni || alumni.length === 0){
                Session.set('alumnilist.showSpinner', false);
            }
            return !!alumni || alumni.length === 0 ? alumni : [{}];
        }
    },
    isAdmin: function () { return AuthService.hasRole(Roles.admin); },
    mayCreate: function(){ return AuthService.may('createAlumni'); },
    showFilterClearIcon: function(){
        if(!!Session.get('stringFilter') && Session.get('stringFilter').length !== 0){
            return true;
        } else {
            return false;
        }
    },
    selectedClassPresident: function(){ return Session.get('presidentFilter'); },
    selectedLocation: function(){ return Session.get('location'); },
    showSpinner: function() { return Session.get('alumnilist.showSpinner'); }
});

Template.alumni.events = {
    'click .add-btn': function (e) {
        e.preventDefault();
        Session.set('alumniId', '');
        Router.go('alumniCreate');
    },
    'click .profile-link': function (e) {
        e.preventDefault();
        Router.go('alumniView', {_id: this._id});
    },
    'click .edit-btn': function (e) {
        e.preventDefault();
        Router.go('/alumni/' + this._id + "/edit");
    },
    'click .delete-btn': function (e) {
        e.preventDefault();
        ActionService.store('action', 'Alumni löschen');
        AlumniService.remove(Session.get('selectedAlumn'));
        $('#deleteModal').modal('hide');
    },
    'click .delete-dropdown': function (e) {
        e.preventDefault();
        Session.set('selectedAlumn', $(e.currentTarget).data('id'));
    },
    'click .sortable': function (e) {
        e.preventDefault();

        Session.set('sortBy', $(e.currentTarget).data('value'));
        Session.set('ascending', !Session.get('ascending'));

        ActionService.store('action', 'Alumni sortieren nach '+$(e.currentTarget).data('value'));
    },
    'click .invite-dropdown': function(e){
        e.preventDefault();
        Session.set('inviteAlumnId', $(e.currentTarget).data('id'));
    },
    'click .invite-btn': function (e) {
        e.preventDefault();
        var alumnId = Session.get('inviteAlumnId');
        var alumn = Alumni.findOne(alumnId);
        ActionService.store('action', 'Alumni einladen');
        Meteor.call('newUser', alumn._id, function (error, response) {
            $('#inviteModal').modal('hide');
            if(!!error) {
                Notifications.error('Einladung konnte nicht verschickt werden.<br>(Vielleicht ist bereits ein User mit dieser Emailadresse vorhanden)');
                Logger.error("invite failed", error);
            } else {
                Notifications.success('Einladung erfolgreich verschickt');
            }
        });
    },
    'click .import-btn': function (e) {
        e.preventDefault();
        Router.go('/alumnimport');
    },
    'click .export-btn': function (e) {
        e.preventDefault();
        CsvService.export();
    },
    /*'change select.location': function (e) {
        e.preventDefault();
        //console.log("select location", $(e.currentTarget).val());
        ActionService.store('action', 'Alumni nach Lokation filtern');
        Session.set('locationFilter', $(e.currentTarget).val());
    },
    'change select.class-presidents': function (e) {
        e.preventDefault();
        ActionService.store('action', 'Alumni nach Jahrgangsprecher filtern');
        var classPresident = $(e.currentTarget).val();
        if(classPresident === '0'){
            classPresident = undefined;
        }

        Session.set('presidentFilter', classPresident);

    },
    'keyup .stringfilter': function (e) {
        e.preventDefault();
        ActionService.store('action', 'Alumni Liste nach String durchsuchen');
        var stringfilter = $(e.currentTarget).val();
        if(stringfilter === ''){
            stringfilter = undefined;
        }
        Session.set('stringFilter', stringfilter);
        AlumniService.fetchAlumni();
    },*/
    'click .clear-filter-js': function (e) {
        e.preventDefault();
        ActionService.store('action', 'Alumni Suche zurücksetzen');
        $('.stringfilter').val('');
        Session.set('stringFilter', undefined);
    },
    'click .update-btn-js': function(e){
        e.preventDefault();
        var allAlumni = Alumni.find({}).fetch();
        allAlumni.forEach(function(alumn){

            //console.log('alumnn', alumn._id);
            Alumni.update({'_id': alumn._id},
                {
                    $set: {
                        'location': {
                            _id: "9fmaEmTuDiSqWsjo6",
                            name: "Wien"
                        }
                    }
                });

            /*if(alumn.lat === undefined || alumn.lat === '') {
                var _this = this;
                VazcoMaps.init({
                    'sensor': true,
                    'key': 'AIzaSyA9mm8lBu01VcFQXGIhh6ld_HzUhvoLZuQ'
                }, function () {
                    _this.mapEngine = VazcoMaps.gMaps();

                    _this.mapEngine.geocode({
                        address: alumn.address,
                        callback: function (results, status) {
                            if (status == 'OK') {
                                var latlng = results[0].geometry.location;
                                alumn.lat = latlng.lat();
                                alumn.lng = latlng.lng();

                                Alumni.update({_id: alumn._id}, alumn, null, function (error, affected) {
                                    //console.log("result", error, affected);
                                });
                            }
                        }
                    });
                });
            }*/
        });
    },
    'click .delete-all-btn': function(e){
        e.preventDefault();
        Meteor.call('deleteAllAlumni', function(error){
            if(error !== undefined){
                console.log("ERROR", error);
            }
        })
    },

    'change #own-created': function(e){
        Session.set('ownFilter', e.target.checked);
    },

    'click .filter-reset-js': function(e){
        e.preventDefault();
        $('.stringfilter').val('');
        Session.set('stringFilter', undefined);

        $('.class-presidents-js')[0].value = '0';
        Session.set('presidentFilter', undefined);
        Router.go('/alumni');
    },

    'click .search-btn-js': function() {
        var query = '?search';
        var classPresident = $('.class-presidents-js').val();
        var stringFilter = $('.stringfilter-js').val();
        var location = $('.location-js').val();
        var hub = $('.hub-js').val();

        if(classPresident !== '0') {
            query += '&classPresident='+classPresident;
        }

        if(stringFilter !== '') {
            query += '&filter='+stringFilter;
        }

        if(location !== '0') {
            query += '&location='+location;
        }

        if(hub !== '0') {
            query += '&hub='+hub;
        }

        Router.go('/alumni' + query);
    },

    'click .toggle-filter-js': (e) => {
        e.preventDefault();
        $('.filter-container-js').toggleClass('hide-on-xs');
        $('.toggle-filter-js').toggleClass('active');
    }
};
