Template.alumniRoles.helpers({
    roles: function() {
        var alumniRoles = AlumniRoles.find().fetch();
        return alumniRoles;
    }
});

Template.alumniRolesEdit.helpers({
    role: function() {
        return AlumniRoles.findOne(Session.get('selectedAlumniRole'));
    },

    title: function(){
        var routeName = Router.current().route.name;
        //console.log("route", routeName);
        if(routeName == 'alumniRolesCreate'){
            //console.log("neu");
            return "Neue Alumni Rolle";
        } else if(routeName == 'alumniRolesEdit'){
            //console.log("edit");
            return 'Alumni Rolle bearbeiten';
        }
        return Router.current().route.name;
    }
});


Template.alumniRoles.events({
    'click .add-btn': function(e, t){
        e.preventDefault();
        Session.set('selectedAlumniRole', null);
        Router.go('/roles/create');
    },
    'click .edit-btn' : function(e){
        e.preventDefault();
        Session.set('selectedAlumniRole', $(e.currentTarget).data('id'));
        Router.go('/roles/'+$(e.currentTarget).data('id')+'/edit');
    },
    'click .save-id': function(e, t){
        e.preventDefault();
        Session.set('selectedAlumniRole', $(e.currentTarget).data('id'));
        //console.log(Session);
    },
    'click .delete-btn': function(e, t){
        e.preventDefault();
        //console.log("foo");
        //console.log(Session);
        AlumniRoles.remove(Session.get('selectedAlumniRole'));
        $('#deleteModal').modal('hide');
    }
});

Template.alumniRolesEdit.events({
    'submit .edit-form': function(e, t){
        e.preventDefault();
        handleRoleSave(e, t);
    },
    'click .save-btn': function(e, t){
        e.preventDefault();
        handleRoleSave(e, t);
    },
    'click .cancel-btn': function(e, t){
        e.preventDefault();
        Router.go('/roles');
    }
})

var handleRoleSave = function(e,t){
    e.preventDefault();
    var data = {};
    $.each($('.role-detail input'), function() {
        data[this.name] = this.value;
    });
    var id = Session.get('selectedAlumniRole');
    if(id) {
        //console.log("alumni role data", data);
        AlumniRoles.update({_id: id}, data, null, function (error, affected) {
            if (!error) {
                Router.go('/roles');
            } else {
                //console.log("ERROR", error, affected);
            }
        });
    } else {
        //console.log("new alumni Role", data);
        AlumniRoles.insert(data);
        Router.go('/roles');
    }
}
