Router.map(function () {
    this.route('alumniRoles', {
        path: '/roles',
        template: 'alumniRoles',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('alumniRoles');
        }
    });
    this.route('alumniRolesCreate', {
        path: '/roles/create',
        template: 'alumniRolesEdit',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        }
    });
    this.route('alumniRolesView', {
        path: '/roles/:_id',
        template: 'alumniRolesView',
        data: function () {
            return Locations.findOne(this.params._id);
        },
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('alumniRoles');
        }
    });
    this.route('alumniRolesEdit', {
        path: '/roles/:_id/edit',
        template: 'alumniRolesEdit',
        data: function () {
            return Locations.findOne(this.params._id);
        },
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('alumniRoles');
        }
    });
});