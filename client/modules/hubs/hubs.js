Template.hubs.helpers({
    hubs: function(){
        Meteor.call('fetchAllHubs', function(error, response){
            if(!!error){
                console.log('ERRÖR', error);
            }

            Session.set('hubs.fetchAllHubs', response);
        });

        return Session.get('hubs.fetchAllHubs');
    },
    superiorHubs: function(){
        Meteor.call('fetchAllSuperiorHubs', function(error, response){
            if(!!error){
                console.log('ERRÖR', error);
            }

            Session.set('hubs.fetchAllSuperiorHubs', response);
        });

        return Session.get('hubs.fetchAllSuperiorHubs');
    }
});