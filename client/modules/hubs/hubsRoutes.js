Router.map(function () {
    this.route('hubs', {
        path: '/hubs',
        template: 'hubs',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('hubs');
            Meteor.subscribe('alumni');
        }
    });
});
