Router.map(function () {

    this.route('/admin/members', {
        name: 'members',
        template: 'members',
        data: function () {
            Session.set('alumniId', this.params._id);
            Session.set('currentTab', 'members');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('alumni', function(){
                Session.set('members.alumni.ready', true);
            });
        }
    });

    this.route('/admin/snapshots', {
        name: 'snapshots',
        template: 'snapshots',
        data: function () {
            Session.set('alumniId', this.params._id);
            Session.set('currentTab', 'snapshots');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            Meteor.subscribe('snapshots');
        }
    });

    this.route('/admin/newalumni', {
        name: 'newAlumni',
        template: 'newAlumni',
        data: function () {
            Session.set('alumniId', this.params._id);
            Session.set('currentTab', 'newAlumni');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){

            var params = {
                selector: {'toBeApproved': true},
                options: {sort: {'lastname': 1}}
            };

            Meteor.subscribe('alumni', params, function(){
                Session.set('newAlumni.ready', true);
            });
        }
    });

    this.route('/admin/activity', {
        name: 'activity',
        template: 'activity',
        data: function () {
            Session.set('alumniId', this.params._id);
            Session.set('currentTab', 'activity');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });

    this.route('/admin/quickedit', {
        name: 'quickEditView',
        template: 'quickEditView',
        data: function () {
            //console.log('data');
            Session.set('alumniId', this.params._id);
            Session.set('currentTab', 'quickEditView');

            Meteor.subscribe('alumni', function(){
                Session.set('quickEditView.alumni.ready', true);
            });
            Meteor.subscribe('locations', function(){
                Session.set('quickEditView.location.ready', true);
            });
            Meteor.subscribe('branches', function(){
                Session.set('quickEditView.branches.ready', true);
            });
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        },
        waitOn: function(){
            console.log('waiton');

        }
    });

    this.route('/admin/branch', {
        name: 'branch',
        template: 'branch',
        data: function () {
            Session.set('currentTab', 'branchView');

            Meteor.subscribe('branches');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });

    this.route('/admin/hub', {
        name: 'hub',
        template: 'hub',
        data: function () {
            Session.set('currentTab', 'hubView');

            Meteor.subscribe('hubs');
            Meteor.subscribe('alumni');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });

    this.route('/admin/event', {
        name: 'event',
        template: 'event',
        data: function () {
            Session.set('currentTab', 'eventView');

            Meteor.subscribe('events');
            Meteor.subscribe('alumni');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });

    this.route('/admin/event/:_id/edit', {
        name: 'eventEdit',
        template: 'eventEdit',
        data: function () {
            Session.set('currentTab', 'eventView');

            Meteor.subscribe('events', this.params._id);
            Meteor.subscribe('alumni');
        },
        onRun: function () {
            if (!Meteor.userId()) {
                Router.go('/login');
            }
        }
    });
});
