Template.newAlumni.helpers({
    alumni: function(){
       return Alumni.find().fetch();
    },
    noAlumniToApprove: function(){
        if(AlumniService.fetchAlumniToBeApproved().length === 0){
            return true;
        } else {
            return false;
        }
    },
    inviteEnabled: function (){
        return ConfigLoader.get('emailToggle') === 'prod';
    }
});

Template.newAlumni.events = {
    'click .alumni-link': function(e){
        e.preventDefault();
        var alumniId = $(e.target).data('id');
        Router.go('alumniView', {_id: alumniId});
    },
    'click #approveModal .invite-js': function(){
        var alumniId = Session.get('selectedEntry');

        ActionService.store('action', 'Bestätige neuen Alumni Eintrag');
        Meteor.call('notifyAdminsAboutApproval', alumniId, function (error) {
            if(!error){
                RequestHandler.call('newUser', alumniId);
            }
        });
        $('#approveModal').modal('hide');
    },
    'click #approveModal .ok-js': function(){
        var alumniId = Session.get('selectedEntry');

        ActionService.store('action', 'Bestätige neuen Alumni Eintrag');
        Meteor.call('notifyAdminsAboutApproval', alumniId, function (error) {
            if(!error){
                AlumniService.approveAlumni(alumniId);
            }
        });
        $('#approveModal').modal('hide');
    },
    'click #approveModal .cancel-btn': function(){
        var alumniId = Session.get('selectedEntry');

        ActionService.store('action', 'Bestätige neuen Alumni Eintrag');
        Meteor.call('notifyAdminsAboutApproval', alumniId, function (error) {
            if(error){
                console.log('error', error);
            } else {
                AlumniService.approveAlumni(alumniId);
            }
        });
        $('#approveModal').modal('hide');
    },
    'click #denyModal .action-btn': function(){
        var alumniId = Session.get('selectedEntry');
        ActionService.store('action', 'Lehne neuen Alumni Eintrag ab');
        Meteor.call('notifyAdminsAboutDenial', alumniId, function (error) {
            if(!error) {
                AlumniService.removeAlumni(alumniId);
            } else {
                console.error(error);
            }
        });
        $('#denyModal').modal('hide');
    },
    'click .action-btn': function(e){
        var alumniId = $(e.target).data('id');
        Session.set('selectedEntry', alumniId);
    }
};