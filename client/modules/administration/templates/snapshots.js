Template.snapshots.helpers({
    snapshots: function(){
        return SnapshotService.fetchAll();
    }
});

Template.snapshots.events = {
    'click .snapshot-list-row-js': function(e){
        var snapshotId = $(e.currentTarget).data('id');
        //console.log('snapshotId', snapshotId);

        Session.set('currentTab', 'snapshotView');
        Session.set('currentSnapshotId', snapshotId);
    }
};
