Template.snapshotView.helpers({
    snapshot: function(){
        var snapshot = SnapshotService.findOne(Session.get('currentSnapshotId'));
        snapshot.alumni = JSON.parse(snapshot.alumni);

        return snapshot;
    }
});