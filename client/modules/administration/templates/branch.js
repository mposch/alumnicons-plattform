Template.branch.helpers({
    rows: function(){
        var alumni = BranchService.fetchBranches();
        return alumni;
    },
    currentBranch: function(){
        return BranchService.findOne(Session.get('currentBranch'));
    }
});

Template.branch.events = {
    'click .new-branch-js': function(e){
        e.preventDefault();
        var branchName = $('#newBranchModal').find('.name-js').val();

        BranchService.save({name: branchName});
        $('#newBranchModal').modal('hide');
    },
    'click .action-btn'     : updateCurrentBranch,
    'click .action-link'    : updateCurrentBranch,
    'click .edit-branch-js': function(e){
        var branchId = $(e.target).data('id');
        var branchName = $('#editBranchModal').find('.name-js').val();

        BranchService.save({_id: branchId, name: branchName});
        $('#editBranchModal').modal('hide');
    },
    'click .delete-branch-js': function(e){
        var branchId = $(e.target).data('id');
        BranchService.delete(branchId);
        $('#deleteBranchModal').modal('hide');
    },
    'click .open-branch-modal-js': function(){
        $('#newBranchModal').find('.name-js').val('');
    }
};

function updateCurrentBranch(e){
    Session.set('currentBranch', $(e.target).data('id'));
}
