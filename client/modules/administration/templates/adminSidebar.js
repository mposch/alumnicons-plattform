Template.adminSidebar.helpers({
    showNewAlumni: function(){
        return Session.get('currentTab') === 'newAlumni';
    },
    showMembers: function(){
        if(!Session.get('currentTab')){
            return true;
        }
        return Session.get('currentTab') === 'members';
    },
    showSnapshots: function(){
        return Session.get('currentTab') === 'snapshots';
    },
    showActivity: function(){
        return Session.get('currentTab') === 'activity';
    },
    showSnapshotView: function(){
        return Session.get('currentTab') === 'snapshotView';
    },
    showQuickEditView: function(){
        return Session.get('currentTab') === 'quickEditView';
    },
    showBranchView: function(){
        return Session.get('currentTab') === 'branchView';
    },
    showHubView: function(){
        return Session.get('currentTab') === 'hubView';
    },
    showEventView: function(){
        return Session.get('currentTab') === 'eventView';
    },
    currentSnapshotId: function(){
        return Session.get('currentSnapshotId');
    },
    isAdmin: function(){
        return AuthService.hasRole(Roles.admin);
    }
});

Template.adminSidebar.events = {
    'click .members-js': function() {
        Router.go('members');
    },
    'click .snapshots-js': function() {
        Router.go('snapshots');
    },
    'click .newalumni-js': function() {
        Router.go('newAlumni');
    },
    'click .activity-js': function() {
        Router.go('activity');
    },
    'click .quickedit-js': function() {
        Router.go('quickEditView');
    },
    'click .branch-js': function() {
        Router.go('branch');
    },
    'click .hub-js': function() {
        Router.go('hub');
    },
    'click .event-js': function() {
        Router.go('event');
    }
};
