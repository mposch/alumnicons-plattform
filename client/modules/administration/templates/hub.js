Template.hub.helpers({
    hubs:           function(){ return HubService.find(); },
    currentHub:     function(){ return HubService.findOne(Session.get('currentHub')); },
    alumni:         function(){ return Alumni.find({}, {sort:{lastname:1}}); },

    entity: function(){
        return {
            name: 'Hub'
        };
    }
});

Template.hub.events = {
    'click .new-js': createEntity,
    'submit .new-form-js': createEntity,

    'click .edit-js': editEntity,
    'submit .edit-form-js': editEntity,

    'click .delete-js': function(e){
        var entityId = $(e.target).data('id');
        HubService.delete(entityId);
        $('#deleteModal').modal('hide');
    },
    'click .open-create-modal-js': function(){
        console.log('create modal');
        $('#newModal').find('.name-js').val('');
        $('#newModal').find('.image-js').val('');
        $('#newModal').find('.position-js').val('');
        $('#newModal').find('.mainhub-js')[0].checked = false;
    },

    'click .action-btn'     : updateCurrentHub,
    'click .action-link'    : updateCurrentHub
};

function createEntity(e){
    e.preventDefault();
    var hubName         = $('#newModal').find('.name-js').val();
    var hubOwner        = $('#newModal').find('.owner-js').val();
    var hubImage        = $('#newModal').find('.image-js').val();
    var hubPosition     = $('#newModal').find('.position-js').val();
    var hubMain         = $('#newModal').find('.mainhub-js')[0].checked;

    var owner = false;
    if(hubOwner) {
        owner = Alumni.findOne({_id: hubOwner}, {fields: {'_id': 1, 'firstname': 1, 'lastname': 1}});
    }

    HubService.save({name: hubName, owner: owner, image: hubImage, position: hubPosition, isMainHub: hubMain});
    $('#newModal').modal('hide');
}

function editEntity(e){
    e.preventDefault();

    var hubId           = Session.get('currentHub');
    var hubName         = $('#editModal').find('.name-js').val();
    var hubOwner        = $('#editModal').find('.owner-js').val();
    var hubImage        = $('#editModal').find('.image-js').val();
    var hubPosition     = $('#editModal').find('.position-js').val();
    var hubMain         = $('#editModal').find('.mainhub-js')[0].checked;

    var owner = false;
    if(hubOwner){
        owner = Alumni.findOne({_id: hubOwner}, {fields: {'_id': 1, 'firstname': 1, 'lastname': 1}});
    }

    const hubData = {_id: hubId, name: hubName, owner: owner, image: hubImage, position: hubPosition, isMainHub: hubMain};
    HubService.save(hubData);
    $('#editModal').modal('hide');
}

function updateCurrentHub(e){
    Session.set('currentHub', $(e.target).data('id'));
}
