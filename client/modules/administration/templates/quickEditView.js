var changedAlumni = {};

Session.set('updatedCount', 0);
Session.set('toBeUpdatedCount', 0);

Template.quickEditView.helpers({
    rows: function() {
        console.log('get rows');
        if(!Session.get('enableRows') || Session.get('enableRows') === 'enabled') {
            console.log('really get rows');
            return Alumni.find().fetch();
        }

        //if(Session.get('quickEditView.alumni.ready')
        //    && Session.get('quickEditView.location.ready')
        //    && Session.get('quickEditView.branches.ready')) {

        //    var alumni = AlumniService.fetchAlumni();
        //    console.log('ALLUUMMNII', alumni);
        //    return alumni;

            //return Alumni.find({_id: '8KnQJAeDX6dWpbgJ4'});
            //return Alumni.find();
        //}


        /*var alumni = [];
        Meteor.call('fetchAllAlumni', function(error, result){
            Session.set('quickEditView.alumni', result);
        });
        return Session.get('quickEditView.alumni');*/


        //console.log('movies', Movies.find().fetch());
        //return Movies.find();
    },
    attributes: function() {
        return [
            {label:'Vorname', styleClass: 'col-firstname'},
            {label:'Nachname', styleClass: 'col-lastname'},
            {label:'Jahrgangssprecher', styleClass: 'col-classPresident'},
            {label:'Standort', styleClass: 'col-location'},
            {label:'Email', styleClass: 'col-emailaddress'},
            {label:'Email (alternativ)', styleClass: 'col-alternativeemailaddress'},
            {label:'Telefon', styleClass: ''},
            {label:'Telefon (alternativ)', styleClass: ''},
            {label:'Adresse', styleClass: 'col-address'},
            {label:'Adresse (alternativ)', styleClass: 'col-alternativeaddress'},
            {label:'Unternehmen', styleClass: 'col-employer'},
            {label:'Job', styleClass: ''},
            {label:'Branche', styleClass: ''},
            {label:'Seit', styleClass: ''},
            {label:'Linkedin', styleClass: ''},
            {label:'Xing', styleClass: ''},
            {label:'icons Start', styleClass: ''},
            {label:'icons Ende', styleClass: ''}
        ];
    },
    showLoadingSpinner: function(){
        return Session.get('quickEditView.showLoadingSpinner');
    },
    isAdmin: function () { return AuthService.hasRole(Roles.admin); }
});

Template.quickEditViewRow.helpers({
    locations       : function() { return Locations.find(); },
    branches        : function() { return Branches.find(); },
    classPresidents : function() { return AlumniService.fetchClassPresidents(); }
});

Template.quickEditView.events = {
    'click .action-btn': function (e) {
        e.preventDefault();
        Session.set('enableRows', 'disabled');
        console.log('enableRows', Session.get('enableRows'));

        console.log('start saving >>', moment().format('HH:mm:ss:SSS'));
        Alumni.update({_id: '8KnQJAeDX6dWpbgJ4'}, {$set: {jobSince: 'foo'}}, function(error, result){
            console.log('AlumniService: update callback, finished updating >>', moment().format('HH:mm:ss:SSS'));
            Session.set('enableRows', 'enabled');
            console.log('enableRows', Session.get('enableRows'));
        });

        /*Meteor.call('saveAlumn', {_id: '8KnQJAeDX6dWpbgJ4', jobSince: 'foobar'}, function(error, result){
            console.log('AlumniService: meteor call callback, finished updating >>', moment().format('HH:mm:ss:SSS'));
        });*/

       /* Meteor.call('saveMovie', function(error, result){
            console.log('after saveMovie');
        });*/

        /*Meteor.call('saveAlumnDummy', function(error, result){
            console.log('blabla >> ', moment().format('HH:mm:ss:SSS'));
        });*/


        //TODO use local vars
       /* Session.set('quickEditView.showLoadingSpinner', true);
        Session.set('updatedCount', 0);
        Session.set('toBeUpdatedCount', Object.keys(changedAlumni).length);

        //persist changed data
        _.each(changedAlumni, function (alumnData, index) {
            alumnData._id = index;
            console.log('QuickEdit: before save >>', moment().format('HH:mm:ss:SSS'));
            AlumniService.saveAlumn(alumnData, {sendNotifications: false}, function() {
                console.log('QuickEdit: savealumni callback', moment().format('HH:mm:ss:SSS'));
                Session.set('quickEditView.showLoadingSpinner', false);
                Session.set('updatedCount', (Session.get('updatedCount') + 1));

                if(Session.get('toBeUpdatedCount') === Session.get('updatedCount')){
                    Notifications.success('Daten erfolgreich gespeichert');
                    Session.set('toBeUpdatedCount', false);
                }
            });
        });*/
    },
    'keyup .grid__cell': updateCurrentValues,
    'change .location': updateCurrentValues,
    'change .classPresident': updateCurrentValues,
    'change .jobbranch': function(e){
        var gridRow = $(e.currentTarget).closest('.grid__row');
        var alumniId = gridRow.data('id');
        changedAlumni[alumniId] = getCurrentValues(gridRow);
    },
    'click .update-all-js': function(){
        var alumni = AlumniService.fetchAlumniForAdministration();

        var index = 0;
        var maxIndex = alumni.length;
        setInterval(function(){
            if(index > maxIndex) {
                return;
            }

            if(!alumni[index].location || !alumni[index].location._id) {
                alumni[index].location = '7JaWjftbsHx3v3FhY';
                AlumniService.saveAlumn(alumni[index], {sendNotifications:false}, function () {
                });
            }

            ++index;
        }, 500);
    }
};

function updateCurrentValues(e){
    var gridRow = $(e.currentTarget).closest('.grid__row');
    var alumniId = gridRow.data('id');
    changedAlumni[alumniId] = getCurrentValues(gridRow);
}

var getCurrentValues = function(parentElement) {
    return {
        firstname               : $(parentElement).find('.firstname').val(),
        lastname                : $(parentElement).find('.lastname').val(),
        mailaddress             : $(parentElement).find('.mailaddress').val(),
        alternativemailaddress  : $(parentElement).find('.alternativemailaddress').val(),
        telephone               : $(parentElement).find('.telephone').val(),
        alternativetelephone    : $(parentElement).find('.alternativetelephone').val(),
        address                 : $(parentElement).find('.address').val(),
        alternativeaddress      : $(parentElement).find('.alternativeaddress').val(),
        employer                : $(parentElement).find('.employer').val(),
        job                     : $(parentElement).find('.job').val(),
        jobSince                : $(parentElement).find('.jobSince').val(),
        jobbranch               : $(parentElement).find('.jobbranch').val(),
        linkedin                : $(parentElement).find('.linkedin').val(),
        xing                    : $(parentElement).find('.xing').val(),
        iconsStart              : $(parentElement).find('.iconsStart').val(),
        iconsEnd                : $(parentElement).find('.iconsEnd').val(),
        location                : $(parentElement).find('.location').val(),
        classPresident          : $(parentElement).find('.classPresident').val()
    };
};
