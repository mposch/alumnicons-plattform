Template.event.rendered = () => {
    const datePickerOptions = {
        inline: true,
        sideBySide: true,
        format: 'DD.MM.YYYY HH:mm',
        calendarWeeks: true,
        useCurrent: true
    };

    $('.has-datepicker-js').datetimepicker(datePickerOptions);
};

Template.event.helpers({
    rows:           function(){ return EventService.find(); },
    currentEvent:   function(){ return EventService.findOne(Session.get('currentEvent')); },
    alumni:         function(){ return Alumni.find({}, {sort:{lastname:1}}); },

    entity: function(){
        return {
            name: 'Event'
        };
    },

    currentLocation: () => {
        return Session.get('event.location');
    }
});

Template.event.events = {
    'click .new-js': createEntity,
    'submit .new-form-js': createEntity,

    'click .edit-js': editEntity,
    'submit .edit-form-js': editEntity,

    'click .delete-js': function(e){
        var entityId = $(e.target).data('id');
        EventService.delete(entityId);
        $('#deleteModal').modal('hide');
    },
    'click .open-create-modal-js': cleanModalForm,

    'click .action-btn'     : updateCurrentEvent,
    'click .action-link'    : updateCurrentEvent,
    'change #newModal .location-js': () => {
        Session.set('event.location', $('#newModal .location-js').val());
    },
    'change #editModal .location-js': () => {
        Session.set('event.location', $('#editModal .location-js').val());
    },
    'click .open-create-modal-js': () => {
        Session.set('event.location', '');
    },
    'click .open-edit-modal-js': () => {
        const eventDate = EventService.findOne(Session.get('currentEvent')).date;
        const dateToUse = !!eventDate ? EventService.findOne(Session.get('currentEvent')).date : moment().toDate();

        $('#editModal .has-datepicker-js').data('DateTimePicker').date(dateToUse);
    }
};

function createEntity(e){
    e.preventDefault();
    var eventTitle      = $('#newModal').find('.title-js').val();
    var eventOwner      = $('#newModal').find('.owner-js').val();
    var eventImage      = $('#newModal').find('.image-js').val();
    var eventLocation   = $('#newModal').find('.location-js').val();
    var eventDate       = $('#newModal').find('.date-js').data('date');
    var eventImagesUrl  = $('#newModal').find('.images-url-js').val();

    var owner = false;
    if(eventOwner) {
        owner = Alumni.findOne({_id: eventOwner}, {fields: {'_id': 1, 'firstname': 1, 'lastname': 1}});
    }

    EventService.save({
        title: eventTitle,
        owner: owner,
        image: eventImage,
        location: eventLocation,
        date: eventDate,
        imagesUrl: eventImagesUrl
    });
    $('#newModal').modal('hide');
}

function editEntity(e){
    e.preventDefault();

    var eventId = Session.get('currentEvent');
    var eventTitle      = $('#editModal').find('.title-js').val();
    var eventOwner      = $('#editModal').find('.owner-js').val();
    var eventImage      = $('#editModal').find('.image-js').val();
    var eventLocation   = $('#editModal').find('.location-js').val();
    var eventDate       = $('#editModal').find('.date-js').data('date');
    var eventImagesUrl  = $('#editModal').find('.images-url-js').val();

    var owner = false;
    if(eventOwner){
        owner = Alumni.findOne({_id: eventOwner}, {fields: {'_id': 1, 'firstname': 1, 'lastname': 1}});
    }

    EventService.save({
        _id: eventId,
        title: eventTitle,
        owner: owner,
        image: eventImage,
        location: eventLocation,
        date: eventDate,
        imagesUrl: eventImagesUrl
    });
    $('#editModal').modal('hide');
}

function updateCurrentEvent(e){
    Session.set('currentEvent', $(e.target).data('id'));
    if(Session.get('currentEvent')) {
        Session.set('event.location', EventService.findOne(Session.get('currentEvent')).location);
    }
}

function cleanModalForm() {
    $('#newModal').find('.title-js').val('');
    $('#newModal').find('.image-js').val('');
    $('#newModal').find('.location-js').val('');
    $('#newModal').find('.date-js').val('');
    $('#newModal').find('.images-url-js').val('');
}