Template.members.helpers({
    alumni: function(){
        //return AlumniService.fetchAlumniForAdministration();
        var alumni = AlumniService.fetchAllRawAlumni();
        return alumni;
    }
});

Template.members.events = {
    'click .alumni-link': function(e){
        e.preventDefault();
        var alumniId = $(e.target).data('id');
        Router.go('alumniView', {_id: alumniId});
    },
    'click .activation-link': function(e){
        e.preventDefault();
        var alumniId = $(e.target)[0].dataset.id;
        var isClubMember = $(e.target)[0].dataset.clubmember;

        isClubMember = !isClubMember;

        ActionService.store('action', 'Alumni aktivieren');
        AlumniService.setClubMembership(alumniId, isClubMember);
    },
    'click .list-body .year-col': function(e){
        var alumnId = $(e.target).data('id');
        var year = $(e.target).data('year');
        var alumn = AlumniService.findById(alumnId);

        var memberShip = alumn.memberShip;
        if(!memberShip){
            if(year === 2014){
                memberShip = [
                    {
                        year: 2015,
                        isActive: false
                    },
                    {
                        year: 2014,
                        isActive: true
                    }
                ]
            } else if(year === 2015){
                memberShip = [
                    {
                        year: 2015,
                        isActive: true
                    },
                    {
                        year: 2014,
                        isActive: false
                    }
                ]
            }
        } else {
            memberShip.forEach(function(memberShipInfo){
                if(memberShipInfo.year === year){
                    memberShipInfo.isActive = !memberShipInfo.isActive;
                }
            });
        }
        ActionService.store('action', 'Ändere Alumni Mitgliedschaft für '+alumnId+' für Jahr '+year);
        AlumniService.setAlumnMemberShips(alumnId, memberShip);
    },
    'click #createSnapshotModal .action-btn': function(e) {
        var alumni = AlumniService.fetchAlumniForAdministration();
        var createdAt = moment().format(Formats.DATETIME);
        var alumniCount = alumni.length;
        var note = $('#createSnapshotModal').find('.snapshot-note').val();
        var memberCount = AlumniService.countMembers(alumni);

        var data = {
            alumni: JSON.stringify(alumni),
            createdAt: createdAt,
            alumniCount: alumniCount,
            memberCount: memberCount,
            note: note
        };

        SnapshotService.save(data);
    }
}