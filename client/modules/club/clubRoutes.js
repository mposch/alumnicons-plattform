Router.map(function () {
    this.route('club', {
        path: '/club',
        template: 'club',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            }
        }
    });
});
