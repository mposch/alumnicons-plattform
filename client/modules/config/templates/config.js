Session.set('loginError', false);

Template.config.events({
    'click #createConfigModal .action-btn': function(e){
        e.preventDefault();
        var key = $('.js-key').val();
        var value = $('.js-value').val();

        My2Config.insert({key: key, value : value}, function(err){
            if (err) {
                //console.log("error when creating user", err);
                // Inform the user that account creation failed
            } else {
                //console.log("user created");
                $('#createConfigModal').modal('hide');
                // Success. Account has been created and the user
                // has logged in successfully.
            }

        });
    }
});