Template.configtable.helpers({
    selectedConfig: function(){
        return My2Config.findOne(Session.get('selectedConfig'));
    },
    rendered: function() {
        $(window).on('keydown', function (e) {
            console.log(e.which);
        })
    },
    config: function(){
        return My2Config.find({}, {sort:{createdAt:-1}}).fetch();
    }
});

Template.configtable.events({
    'click #deleteModal .action-btn': function (e) {
        e.preventDefault();
        My2Config.remove(Session.get('selectedConfig'));
        $('#deleteModal').modal('hide');
    },
    'click .btn': function (e) {
        e.preventDefault();
        if ($(e.currentTarget).data('id')) {
            Session.set('selectedConfig', $(e.currentTarget).data('id'));
        }
    },
    'click .actions a': function (e) {
        e.preventDefault();
        if ($(e.currentTarget).data('id')) {
            Session.set('selectedConfig', $(e.currentTarget).data('id'));
        }
    },
    'click #editConfigModal .action-btn': function (e) {
        e.preventDefault();
        var id = Session.get('selectedConfig');
        var key = $('#editConfigModal .js-key-edit').val();
        var value = $('#editConfigModal .js-value-edit').val();

        My2Config.update({'_id': id}, {$set: {'key': key, 'value': value}}, function(error, response){
            //console.log("server response", error, response);
            if(!error){
                $('#editConfigModal').modal('hide');
            } else {
                console.log("ERROR", error, affected);
            }
        });
    }
});