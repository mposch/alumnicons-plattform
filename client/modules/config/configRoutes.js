Router.map(function () {
    this.route('config', {
        path: '/config',
        template: 'config',
        onRun: function () {
            if (!Meteor.userId()) {
                this.redirect('/login');
            } else {
                ActionService.store('navigate', 'User Liste');
                if (!AuthService.hasRole(Roles.admin)) {
                    this.redirect('/alumni');
                }
            }
        }
    });
});