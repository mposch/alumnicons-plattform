Router.map(function () {
    this.route('users', {
        path: '/users',
        template: 'users',
        onRun: function () {
            if (!Meteor.userId()) {
                this.redirect('/login');
            } else {
                ActionService.store('navigate', 'User Liste');
                if (!AuthService.hasRole(Roles.admin)) {
                    this.redirect('/alumni');
                }
            }
        },
        waitOn: function(){
            Meteor.subscribe('alumni');
        }
    });

    this.route('userRoles', {
        path: '/userroles',
        template: 'userRoles',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                ActionService.store('navigate', 'User Rollen');
                if (!AuthService.hasRole(Roles.admin)) {
                    this.render('alumni');
                }
            }
        }
    });

    this.route('userRolesCreate', {
        path: '/userroles/create',
        template: 'userRolesEdit',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                ActionService.store('navigate', 'User Rollen anlegen');
                if (!AuthService.hasRole(Roles.admin)) {
                    this.render('alumni');
                }
            }
        }
    });

    this.route('userRolesEdit', {
        path: '/userroles/:_id/edit',
        template: 'userRolesEdit',
        data: function () {
            return Meteor.users.findOne(this.params._id);
        },
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                ActionService.store('navigate', 'User Rollen bearbeiten');
                if (!AuthService.hasRole(Roles.admin)) {
                    this.render('alumni');
                }
            }
        }
    });

    this.route('activation', {
        path: '/activation/:_id/:token',
        template: 'activation',
        data: function () {
            Session.set('activationUserId', this.params._id);
            Session.set('activationToken', this.params.token);
            var user = Meteor.users.findOne(this.params._id);
            var templateData = {};
            if (user) {
                templateData.email = user.emails[0].address;

                var alumn = AlumniService.findByUserId(user._id);
                if(!!alumn){
                    templateData.firstname = alumn.firstname;
                    templateData.lastname = alumn.lastname;
                }
            }
            return templateData;
        },
        onRun: function(){
            ActionService.store('navigate', 'User Aktivierung');
        },
        waitOn: function(){
            Meteor.subscribe('alumni');
        }
    });

    this.route('resetPassword', {
        path: '/resetpassword',
        template: 'resetPassword'
    });

    this.route('setPassword', {
        path: '/resetpassword/:_id/:token',
        template: 'setPassword',
        data: function() {
            Session.set('userId', this.params._id);
            Session.set('setPasswordToken', this.params.token);
        },
        onRun: function(){
            ActionService.store('navigate', 'User Passwort zurücksetzen');
        }
    });

    this.route('login', {
        path: '/login',
        template: 'login',
        layoutTemplate: 'loginLayout',
        onRun: function() {
            ActionService.store('navigate', 'Login');
        }
    });
});