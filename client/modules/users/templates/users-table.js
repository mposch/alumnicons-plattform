Template.userstable.helpers({
    selectedUser: function(){
        return UserService.findOne(Session.get('selectedUser'));
    },
    rendered: function() {
        $(window).on('keydown', function (e) {
            console.log(e.which);
        })
    },
    roles: function(){
        return UserService.fetchAllRoles();
    },
    alumni: function(){
        return AlumniService.fetchAllRawAlumni();
    },
    users: function(){
        var users = UserService.fetchAllUsers();
        return users;
    },
    maySeeEditUserShortCut: function(){
        return true;
    }
});

Template.userstable.events({
    'click #deleteModal .action-btn': function (e) {
        e.preventDefault();
        UserService.deleteById(Session.get('selectedUser'));
        $('#deleteModal').modal('hide');
    },
    'click .btn': function (e) {
        e.preventDefault();
        if ($(e.currentTarget).data('id')) {
            Session.set('selectedUser', $(e.currentTarget).data('id'));
        }
    },
    'click .actions a': function (e) {
        e.preventDefault();
        if ($(e.currentTarget).data('id')) {
            Session.set('selectedUser', $(e.currentTarget).data('id'));
        }
    },
    'click #setRoleModal .action-btn': function (e) {
        e.preventDefault();
        var role = $('.role-select').val();

        UserService.setRole(Session.get('selectedUser'), role, function (error, affected) {
            if (!error) {
                Router.go('/users/');
            } else {
                console.log("ERROR", error, affected);
            }
        });

        $('#setRoleModal').modal('hide');
    },
    'click #setAlumnModal .action-btn': function (e) {
        e.preventDefault();

        var userId = Session.get('selectedUser');
        var alumnId = $('.alumn-select').val();

        UserService.assignAlumnToUser(userId, alumnId, function (error, affected) {
            if (!error) {
                Router.go('/users/');
            } else {
                console.log("ERROR", error, affected);
            }
        });

        $('#setAlumnModal').modal('hide');
    },
    'click #setPasswordModal .action-btn': function (e) {
        e.preventDefault();
        var userId = Session.get('selectedUser');
        var newPassword = $('.user-password').val();

        UserService.setUserPassword(userId, newPassword, function (error, response) {
            //console.log("server response", error, response);
            if(!error){
                $('#setPasswordModal').modal('hide');
            } else {
                console.log("ERROR", error, affected);
            }
        });
    },
    'click #inviteModal .action-btn': function (e) {
        e.preventDefault();
        var userId = Session.get('selectedUser');

        console.log('1');
        UserService.reinvite(userId, function (err, response) {
            console.log("server response", err, response);
            if(!err){
                $('#inviteModal').modal('hide');
            } else {
                console.log('ERROR', err);
            }
        });
    },
    'click .profile-link': function (e) {
        e.preventDefault();
        var alumnId = e.target.dataset.id;
        Router.go('alumniView', {_id: alumnId});
    },
});