Template.resetPassword.helpers({
    resetPasswordError: function(){
        return Session.get('resetPasswordError');
    },
    resetPasswordErrorMessage: function(){
        return Session.get('resetPasswordErrorMessage');
    },
    resetPasswordSuccess: function(){
        return Session.get('resetPasswordSuccess');
    },
    resetPasswordSuccessMessage: function() {
        return Session.get('resetPasswordSuccessMessage');
    }
});

Template.resetPassword.events({
    'click #trigger-reset-password-button': function(e){
        e.preventDefault();
        Session.set('resetPasswordError', false);
        Session.set('resetPasswordSuccess', false);

        var email = $('#reset-password-email').val();

        if(!email) {
            Session.set('resetPasswordError', true);
            Session.set('resetPasswordErrorMessage', 'Bitte gib eine Emailadresse ein.');
            return;
        }

        Meteor.call('triggerResetPassword', email, function (error, response) {
            if(error || response === false){
                Session.set('resetPasswordError', true);
                Session.set('resetPasswordErrorMessage', 'Emailadresse ist ungültig. Bitte wende dich an admin@alumni.icons.at.');
            } else {
                Session.set('resetPasswordSuccess', true);
                Session.set('resetPasswordSuccessMessage', 'Eine Email mit weiteren Hinweisen wurde an '+email+' gesendet.');
            }
        });
    }
});