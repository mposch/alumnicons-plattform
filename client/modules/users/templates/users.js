Session.set('loginError', false);

Template.users.events({
    'click #createUserModal .action-btn': function(e){
        e.preventDefault();
        var userName = $('.email').val();
        var password = $('.password').val();
        //console.log("create user", userName, password);

        Accounts.createUser({email: userName, password : password, username: userName}, function(err){
            if (err) {
                //console.log("error when creating user", err);
                // Inform the user that account creation failed
            } else {
                //console.log("user created");
                $('#createUserModal').modal('hide');
                // Success. Account has been created and the user
                // has logged in successfully.
            }

        });
    }
});