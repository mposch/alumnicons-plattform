Template.login.helpers({
    loginError: function(){
        return Session.get('loginError');
    }
});

Template.login.events({
    'submit #login-form' : function(e, t){
        e.preventDefault();
        Session.set('loginError', false);

        $('#login-button').addClass('has-progress-indicator');
        $('#login-button').val('');

        var email = t.find('#login-email').value;
        var password = t.find('#login-password').value;

        Meteor.loginWithPassword(email, password, function(err){
            if (err){
                //console.log("loginError", err);
                Session.set('loginError', true);
                $('#login-button').removeClass('has-progress-indicator');
                $('#login-button').val('Login');
            } else {
                //console.log("successful login");
                Router.go('/alumni');
            }
        });

        return false;

    }
});