Template.setPassword.helpers({
    setPasswordError: function(){
        return Session.get('setPasswordError');
    },
    setPasswordErrorMessage: function(){
        return Session.get('setPasswordErrorMessage');
    },
    setPasswordSuccess: function(){
        return Session.get('setPasswordSuccess');
    },
    setPasswordSuccessMessage: function() {
        return Session.get('setPasswordSuccessMessage');
    }
});

Template.setPassword.events({
   'click #set-password-button': function(e){
       e.preventDefault();
       Session.set('setPasswordError', false);
       Session.set('setPasswordSuccess', false);

       var password         = $('#set-password-email').val();
       var passwordCheck    = $('#set-password-email-check').val();
       var userId           = Session.get('userId');

       if(!password || !passwordCheck) {
           Session.set('setPasswordError', true);
           Session.set('setPasswordErrorMessage', 'Beide Passwörter müssen ausgefüllt werden.');
           return;
       }

       if(password !== passwordCheck) {
           Session.set('setPasswordError', true);
           Session.set('setPasswordErrorMessage', 'Die Passwörter stimmen nicht überein.');
           return;
       }

       Meteor.call('validatePasswordToken', userId, Session.get('setPasswordToken'), function(err, response) {
           if (!!err || !response) {
               Session.set('setPasswordError', true);
               Session.set('setPasswordErrorMessage', "Token ist abgelaufen oder ungültig. Bitte wende dich an admin@alumni.icons.at.");
               Logger.error("activation error, invalid password token");
               return false;
           }

           Meteor.call('customSetPassword', userId, password, function (error, response) {
               if (!error && response !== false) {
                   Session.set('setPasswordSuccess', true);
                   Session.set('setPasswordSuccessMessage', 'Passwort wurde erfolgreich geändert. Du kannst dich nun einloggen.');
               } else {
                   Session.set('setPasswordError', true);
                   Session.set('setPasswordErrorMessage', 'Es ist ein Fehler aufgetreten. Bitte wende dich an admin@alumni.icons.at');
               }
           });
       });
   }
});