Template.activation.events({
    'submit #activation-form': function (e, t) {
        e.preventDefault();
        var password = t.find('#account-password').value;
        var checkPassword = t.find('#check-password').value;

        if(password === "" || checkPassword === ""){
            Session.set('activationError', true);
            Session.set('markPasswordFields', true);
            Session.set('activationErrorMessage', "Beide Passwörter müssen ausgefüllt werden!");
            Logger.error("activation error, nicht beide passwörter ausgefüllt");
            return false;
        }

        if(!Session.get('activationToken')){
            Router.go('/login');
            return false;
        }

        Meteor.call('validatePasswordToken', Session.get('activationUserId'), Session.get('activationToken'), function(err, response){
            if(!!err || !response){
                Session.set('activationError', true);
                Session.set('activationErrorMessage', "Token ist abgelaufen oder ungültig. Bitte wende dich an admin@alumni.icons.at.");
                Logger.error("activation error, invalid password token");
                return false;
            }

            if(password === checkPassword){
                Meteor.call('customSetPassword', Session.get('activationUserId'), password, function (err, response) {
                    if(!err){
                        Meteor.loginWithPassword({id: Session.get('activationUserId')}, password, function(err){
                            if(!err){
                                Meteor.call('activateUser', Session.get('activationUserId'), (error) => {
                                    if(!error){
                                        Meteor.call('notifyAdminsAboutActivation', Session.get('activationUserId'), function (error, response) {});
                                    } else {
                                        console.log('activation error', error);
                                    }
                                });
                                Router.go('/alumni');
                            } else {
                                Session.set('activationError', true);
                                Session.set('markPasswordFields', true);
                                Session.set('activationErrorMessage', "Fehler, bitte versuchen Sie es etwas später nocheinmal!");
                                Logger.error("activation error, login error");
                                return false;
                            }
                        });
                    } else {
                        Session.set('activationError', true);
                        Session.set('markPasswordFields', true);
                        Session.set('activationErrorMessage', "Fehler, bitte versuchen Sie es etwas später nocheinmal!");
                        Logger.error("activation error", err);
                        return false;
                    }
                });
            } else {
                Session.set('activationError', true);
                Session.set('markPasswordFields', true);
                Session.set('activationErrorMessage', "Passwörter sind nicht gleich!");
                Logger.error("activation error, passwörter sind nicht gleich");
                return false;
            }
        });
    }
});

Template.activation.helpers({
    activationError: function(){
        return Session.get('activationError');
    },
    activationErrorMessage: function(){
        return Session.get('activationErrorMessage');
    },
    markPasswordFields: function(){
        return Session.get('markPasswordFields');
    }
});
