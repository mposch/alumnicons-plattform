Template.userRoles.helpers({
    roles: function() {
        var roles = UserRoles.find({},{sort: {name:1}}).fetch();
        return roles;
    },
    userRole: function(){
        var role = UserRoles.findOne({'_id':Session.get('selectedUserRoleId')});
        return role;
    }
});

Template.userRoles.events({
    'click .add-btn': function(e){
        e.preventDefault();
        Session.set('selectedRole', null);
        Router.go('/userroles/create');
    },
    'click .edit-btn' : function(e){
        e.preventDefault();
        Session.set('selectedRole', $(e.currentTarget).data('id'));
        Router.go('/userroles/'+$(e.currentTarget).data('id')+'/edit');
    },
    'click .save-id': function(e){
        e.preventDefault();
        Session.set('selectedRole', $(e.currentTarget).data('id'));
        console.log(Session);
    },
    'click #deleteModal .action-btn': function(e){
        e.preventDefault();
        UserRoles.remove(Session.get('selectedUserRoleId'));
        $('#deleteModal').modal('hide');
    },
    'click #editModal .action-btn': function(e){
        e.preventDefault();

        var roleId = Session.get('selectedUserRoleId');
        var roleName = $('#editModal').find('.name').val();

        //console.log(roleId, roleName);

        UserRoles.update({_id: roleId}, {name: roleName}, null, function (error, affected) {
            if (!error) {
                $('#editModal').modal('hide');
            } else {
                console.log("ERROR", error, affected);
            }
        });
    },
    'click .action-link': function(e){
        e.preventDefault();
        //console.log('action link', $(e.currentTarget).data('id'));
        var userRoleId = $(e.currentTarget).data('id');
        Session.set('selectedUserRoleId', userRoleId);
    },
    'click #createRoleModal .action-btn': function(e){
        var roleName = $('#createRoleModal').find('.name').val();
        //console.log('roleName', roleName);
        UserRoles.insert({name: roleName});
        $('#createRoleModal').modal('hide');
    }
});
