Router.map(function () {
    this.route('locations', {
        path: '/locations',
        template: 'locations',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('locations');
        }
    });
    this.route('locationsCreate', {
        path: '/locations/create',
        template: 'locationsEdit',
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        }
    });
    this.route('locationsView', {
        path: '/locations/:_id',
        template: 'locationsView',
        data: function () {
            return Locations.findOne(this.params._id);
        },
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('locations');
        }
    });

    this.route('locationsEdit', {
        path: '/locations/:_id/edit',
        template: 'locationsEdit',
        data: function () {
            return Locations.findOne(this.params._id);
        },
        onRun: function () {
            if (!Meteor.userId()) {
                this.render('login');
            } else {
                if (!AuthService.hasRole('admin')) {
                    this.render('alumni');
                }
            }
        },
        waitOn: function() {
            Meteor.subscribe('locations');
        }
    });
});