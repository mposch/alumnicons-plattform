Template.locations.helpers({
    locations: function() {
        var locations = Locations.find();
        return locations;
    }
});

Template.locations.events({
    'click .add-btn': function(e, t){
        e.preventDefault();
        Router.go('/locations/create');
    },
    'click .edit-btn' : function(e){
        e.preventDefault();
        //console.log("edit", $(e.currentTarget).data('id'));
        Router.go('/locations/'+$(e.currentTarget).data('id')+'/edit');
    },
    'click .save-id': function(e, t){
        e.preventDefault();
        //console.log("select", $(e.currentTarget).data('id'));
        Session.set('selectedLocation', $(e.currentTarget).data('id'));
        //console.log(Session);
    },
    'click .delete-btn': function(e, t){
        e.preventDefault();
        //console.log("foo");
        //console.log(Session);
        Locations.remove(Session.get('selectedLocation'));
        $('#deleteModal').modal('hide');
    }
});

Template.locationsEdit.events({
    'click .save-btn': function(e, t) {
        e.preventDefault();
        var data = {};
        $.each($('.location-detail input'), function() {
            //console.log(this.name, this.value);
            data[this.name] = this.value;
        });
        var id = $(e.currentTarget).data('id');
        if(id) {
            //console.log("alumn data", data);
            Locations.update({_id: $(e.currentTarget).data('id')}, data, null, function (error, affected) {
                if (!error) {
                    Router.go('/locations');
                } else {
                    console.log("ERROR", error, affected);
                }
            });
        } else {
            //console.log("new location");
            Locations.insert(data);
            Router.go('/locations');
        }
    },
    'click .cancel-btn': function(e, t){
        e.preventDefault();
        Router.go('/locations');
    }
})
